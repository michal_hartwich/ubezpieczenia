﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controls
{
    public class TxtBox : System.Windows.Forms.TextBox
    {

        private bool edited;

        public bool Edited
        {
            set
            {
                edited = value;
            }
            get
            {
                return edited;
            }
        }

        public TxtBox()
        {
            this.Enter += new System.EventHandler(this.this_Enter);
            this.Leave += new System.EventHandler(this.this_Leave);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.this_KeyDown);
            this.CharacterCasing = CharacterCasing.Upper;
        }

        private void this_Enter(object sender, EventArgs e)
        {
            this.Select(0, 0);
            this.BackColor = System.Drawing.Color.LemonChiffon;
        }

        private void this_Leave(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.SystemColors.Window;
        }

        private void this_KeyDown(object sender, KeyEventArgs e)
        {
            this.Edited=true;
        }
    }
}
