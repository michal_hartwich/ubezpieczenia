﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using Klienci;
using StaticRegistry;
using Elements;

// próba repo

namespace Ubezpieczenia
{
    public partial class Form1 : Form
    {

        private DataTable klienci;
        private DataTable produkty;
        private DataTable przedmioty;
        private DataTable polisy;
        private DataTable grupy;
        private bool typeEdit = false;
        private int currEditItem;

        public Form1()
        {
            InitializeComponent();
            string sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Ubezpieczenia.Properties.Settings.databaseConnectionString"].ConnectionString;
            //Registry.SqlConn = new SqlConnection(sqlConnectionString);
            Registry.ConnString = sqlConnectionString;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DodajKlienta nowy = new DodajKlienta();
            nowy.ShowDialog();
            Form1_Load(null, null);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'databaseDataSet.PrzedmiotyGridView' table. You can move, or remove it, as needed.
            this.przedmiotyGridViewTableAdapter.Fill(this.databaseDataSet.PrzedmiotyGridView);
            // TODO: This line of code loads data into the 'databaseDataSet.Przedmioty' table. You can move, or remove it, as needed.
            this.przedmiotyTableAdapter.Fill(this.databaseDataSet.Przedmioty);
            // TODO: This line of code loads data into the 'databaseDataSet.Grupy' table. You can move, or remove it, as needed.
            this.grupyTableAdapter.Fill(this.databaseDataSet.Grupy);
            // TODO: This line of code loads data into the 'databaseDataSet.ProdKlienta' table. You can move, or remove it, as needed.
            this.prodKlientaTableAdapter.Fill(this.databaseDataSet.ProdKlienta);
            // TODO: This line of code loads data into the 'databaseDataSet.Produkty' table. You can move, or remove it, as needed.
            this.produktyTableAdapter.Fill(this.databaseDataSet.Produkty);
            // TODO: This line of code loads data into the 'databaseDataSet.Klienci' table. You can move, or remove it, as needed.
            this.klienciTableAdapter.Fill(this.databaseDataSet.Klienci);
            // TODO: This line of code loads data into the 'databaseDataSet2.ProdKlienta' table. You can move, or remove it, as needed.
            this.polisyTableAdapter2.Fill(this.databaseDataSet.Polisy);
            this.produkty = this.databaseDataSet.Produkty;
            this.klienci = this.databaseDataSet.Klienci;
            this.przedmioty = this.databaseDataSet.ProdKlienta;
            this.grupy = this.databaseDataSet.Grupy;
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();

                String strSQL = "SELECT * FROM Polisy ;";
                SqlDataAdapter da = new SqlDataAdapter(strSQL, conn);
                polisy = new DataTable();
                da.Fill(polisy);//<-there is an exception
                //prodKlientaBindingSource.DataSource = klienci;
                //dataGridView3.AutoGenerateColumns = false;
                //dataGridView3.DataSource = prodKlientaBindingSource;
                //dataGridView3.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

            ////    String strSQL2 = "SELECT * FROM Produkty;";
            ////    SqlDataAdapter da2 = new SqlDataAdapter(strSQL2, conn);
            ////    produkty = new DataTable();
            ////    da2.Fill(produkty);//<-there is an exception
            ////    produktyBindingSource.DataSource = produkty;
            ////    dataGridView2.AutoGenerateColumns = false;
            ////    dataGridView2.DataSource = produktyBindingSource;
            ////    dataGridView2.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

                conn.Close();
            }
            //if (polisy.Rows.Count > 0)
            //{
            //    dataGridView3.Rows.Clear();
            //    foreach (DataRow rowPrzedmioty in databaseDataSet.ProdKlienta)
            //    {
                    
            //        List<String> datarow = new List<String>();
            //        DataRow[] polisaIdRow = polisy.Select("Id_polisy='" + rowPrzedmioty[1].ToString() + "'");
            //        //MessageBox.Show(polisaIdRow[0]["Id_klienta"].ToString());
            //        DataRow[] klientRow = klienci.Select("Id_klienta='" + polisaIdRow[0]["Id_klienta"].ToString() + "'");

            //        datarow.Add(rowPrzedmioty[0].ToString());
            //        datarow.Add(rowPrzedmioty[1].ToString());
            //        datarow.Add(klientRow[0]["Nazwisko"] + " " + klientRow[0]["Imie"]);

            //        DataRow[] typRow = produkty.Select("Id_produktu='" + rowPrzedmioty["Id_typu"].ToString() + "'");
            //        datarow.Add(typRow[0]["Rodzaj"].ToString());
            //        int i = 3;
            //        while(i< 10)
            //        {
            //            datarow.Add(rowPrzedmioty[i].ToString());
            //            i++;
            //        }
            //        if (rowPrzedmioty[11].ToString().Equals("0"))
            //        {
            //            datarow.Add("Wznowiona");
            //        }
            //        else
            //        {
            //            datarow.Add("Nowa");
            //        }
                    
            //        //ListViewItem item = new ListViewItem(datarow.ToArray());
            //        dataGridView3.Rows.Add(datarow.ToArray());
            //        //this.polisyList.Items.Add(item);
            //    }
            //}
        }

        private void Form1_EnabledChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = (int) this.dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            EdytujKlienta edit = new EdytujKlienta();
            edit.ID = id;
            edit.ShowDialog();
            Form1_Load(null, null);
        }

    
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                //MessageBox.Show(this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells[0].Value.ToString());
                e.Handled = true;
                int id = (int)this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells[0].Value;
                EdytujKlienta edit = new EdytujKlienta();
                edit.ID = id;
                edit.ShowDialog();
                Form1_Load(null, null);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.button2.Text = "Dodaj";
            this.addTypePanel.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.addTypePanel.Visible = false;
            this.rodzajTxt.Text = "";
            this.prowizjaTxt.Text = "";
            this.opisRich.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (rodzajTxt.Text != "")
            {
                Grupa prod = new Grupa();
                prod.Rodzaj = rodzajTxt.Text;
                prod.Opis = opisRich.Text;
                prod.Prowizja = prowizjaTxt.Text;
                if (this.typeEdit)
                {
                    prod.EdytujGrupe(this.currEditItem);
                }
                else
                {
                    prod.DodajGrupe();
                }
                Form1_Load(null, null);
                //using (SqlConnection conn = new SqlConnection(Registry.ConnString))
                //{
                //    conn.Open();
                //    String strSQL2 = "SELECT * FROM Produkty;";
                //    SqlDataAdapter da2 = new SqlDataAdapter(strSQL2, conn);
                //    produkty = new DataTable();
                //    da2.Fill(produkty);//<-there is an exception
                //    produktyBindingSource.DataSource = produkty;
                //    dataGridView2.AutoGenerateColumns = false;
                //    dataGridView2.DataSource = produktyBindingSource;
                //    dataGridView2.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                //    conn.Close();
                //}
                if (!this.dontClose.Checked)
                {
                    this.addTypePanel.Visible = false;
                }
                this.rodzajTxt.Text = "";
                this.prowizjaTxt.Text = "";
                this.opisRich.Text = "";
            }
            else
            {
                MessageBox.Show("Proszę wypełnić pole \"Rodzaj\".", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show(this.dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString());
            this.typeEdit = true;
            this.currEditItem = int.Parse(this.dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString());
            DataRow[] row = grupy.Select("Id_grupy=" + this.dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString());
            //MessageBox.Show(row[0]["Rodzaj"].ToString());
            this.rodzajTxt.Text = row[0]["Rodzaj"].ToString();
            this.prowizjaTxt.Text = row[0]["Prowizja"].ToString();
            this.opisRich.Text = row[0]["Opis"].ToString();
            this.button2.Text = "Zapisz";
            this.addTypePanel.Visible = true;
        }

        private void dataGridView2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                MessageBox.Show(this.dataGridView2.Rows[this.dataGridView2.CurrentCell.RowIndex].Cells[0].Value.ToString());
                e.Handled = true;
            }
        }

        private void btnDodajPolise_Click(object sender, EventArgs e)
        {
            PolisyForm form = new PolisyForm();
            form.ShowDialog();
            Form1_Load(null, null);
        }

        private void polisyList_DoubleClick(object sender, EventArgs e)
        {
            ////PolisyForm frm = new PolisyForm(this.polisyList.SelectedItems[0].SubItems[11].Text, this.polisyList.SelectedItems[0].SubItems[10].Text);
            //frm.Show();
        }

        private void dataGridView3_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            PolisyForm frm = new PolisyForm(this.dataGridView3.Rows[e.RowIndex].Cells[1].Value.ToString(), this.dataGridView3.Rows[e.RowIndex].Cells[0].Value.ToString());
            frm.ShowDialog();
            this.Form1_Load(null, null);
        }

        private void dataGridView3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                //MessageBox.Show(this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells[0].Value.ToString());
                e.Handled = true;
                PolisyForm frm = new PolisyForm(this.dataGridView3.Rows[this.dataGridView3.CurrentCell.RowIndex].Cells[1].Value.ToString(), this.dataGridView3.Rows[this.dataGridView3.CurrentCell.RowIndex].Cells[0].Value.ToString());
                frm.ShowDialog();
                this.Form1_Load(null, null);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            PolisyForm form = new PolisyForm();
            form.ShowDialog();
            Form1_Load(null, null);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            PolisyForm frm = new PolisyForm(this.dataGridView3.Rows[this.dataGridView3.CurrentCell.RowIndex].Cells[1].Value.ToString(), this.dataGridView3.Rows[this.dataGridView3.CurrentCell.RowIndex].Cells[0].Value.ToString());
            frm.ShowDialog();
            this.Form1_Load(null, null);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Czy chcesz usunąć wybrane elementy?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (r == System.Windows.Forms.DialogResult.Yes)
            {
                foreach (DataGridViewRow row in dataGridView3.SelectedRows)
                {
                    ProdKlienta.UsunProdKlienta(row.Cells[0].Value.ToString());
                }
                Form1_Load(null, null);
                
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
             
                 this.Close();
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult r = MessageBox.Show("Czy chcesz zakończyć pracę programu?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (r == System.Windows.Forms.DialogResult.Yes)
            {

            }
            else 
            {
                e.Cancel = true;
                return;
            }
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            this.button2.Text = "Dodaj";
            this.addTypePanel.Visible = true;
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            this.typeEdit = true;
            //this.currEditItem = int.Parse(this.dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString());
            DataRow[] row = produkty.Select("Id_produktu=" + this.dataGridView2.Rows[this.dataGridView2.CurrentCell.RowIndex].Cells[0].Value);
            //MessageBox.Show(row[0]["Rodzaj"].ToString());
            this.rodzajTxt.Text = row[0]["Rodzaj"].ToString();
            this.prowizjaTxt.Text = row[0]["Prowizja"].ToString();
            this.opisRich.Text = row[0]["Opis"].ToString();
            this.button2.Text = "Zapisz";
            this.addTypePanel.Visible = true;
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            
            this.prodKlientaTableAdapter.Fill(this.databaseDataSet.ProdKlienta);
            DataTable table = this.databaseDataSet.ProdKlienta;
            

            DialogResult r = MessageBox.Show("Czy chcesz usunąć wybrane elementy?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (r == System.Windows.Forms.DialogResult.Yes)
            {
                foreach (DataGridViewRow row in dataGridView2.SelectedRows)
                {
                    DataRow[] rows = table.Select("Id_typu=" + row.Cells[0].Value.ToString());
                    if (rows.Length == 0)
                    {
                        Grupa.UsunGrupe(row.Cells[0].Value.ToString());
                    }
                    else
                    {
                        MessageBox.Show("Nie można usunąć typu \"" + row.Cells[1].Value + "\", ponieważ istnieją przedmioty do niego przypisane.", "Ostrzeżenie", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    
                }
                Form1_Load(null, null);

            }
        }

        private void splitContainer2_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            DodajKlienta nowy = new DodajKlienta();
            nowy.ShowDialog();
            Form1_Load(null, null);
        }

        private void toolStripButton10_Click(object sender, EventArgs e)
        {
            int id = (int)this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells[0].Value;
            EdytujKlienta edit = new EdytujKlienta();
            edit.ID = id;
            edit.ShowDialog();
            Form1_Load(null, null);
        }

        private void toolStripButton11_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Czy chcesz usunąć klienta wraz z jego wszystkimi polisami?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (r == System.Windows.Forms.DialogResult.Yes)
            {
                
                DataRow[] polisa = this.databaseDataSet.Polisy.Select("Id_klienta=" + this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells[0].Value);
                
                using (SqlConnection conn = new SqlConnection(Registry.ConnString))
                {
                    conn.Open();
                    foreach (DataRow row in polisa)
                    {
                        SqlCommand command = new SqlCommand("DELETE FROM ProdKlienta WHERE Id_polisy='" + row["Id_polisy"] + "';", conn);
                        command.ExecuteNonQuery();
                        Polisa.UsunPolise(row["Id_polisy"].ToString());
                    }

                    SqlCommand command2 = new SqlCommand("DELETE FROM Klienci WHERE Id_klienta='" + this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells[0].Value + "';", conn);
                    command2.ExecuteNonQuery();
                    //command.Parameters.AddWithValue("@Id_produktu", int.Parse(id));
               
                    
                    conn.Close();
                }
                Form1_Load(null, null);
            }
        }

        private void toolStripButton12_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.przedmiotyTableAdapter.FillBy(this.databaseDataSet.Przedmioty);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillByToolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                this.przedmiotyTableAdapter.FillBy(this.databaseDataSet.Przedmioty);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void toolStripButton13_Click(object sender, EventArgs e)
        {
            this.panel2.Visible = true;
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            if (itemRodzaj.Text=="")
            {
                MessageBox.Show("Proszę wypełnić pole \"Rodzaj\".", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Przedmiot item = new Przedmiot();
            item.Id_grupy = itemGrupa.SelectedValue+"";
            item.Rodzaj = itemRodzaj.Text;
            item.Opis = itemOpis.Text;
            item.DodajPrzedmiot();
            Form1_Load(null, null);
            if (!this.itemOpen.Checked)
            {
                this.panel2.Visible = false;
            }
            this.itemRodzaj.Text = "";
            this.itemOpis.Text = "";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.itemRodzaj.Text = "";
            this.itemOpis.Text = "";
            this.panel2.Visible = false;
        }

        

        

    }
}
