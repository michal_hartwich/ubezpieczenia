﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using StaticRegistry;
using Controls;
using Elements;

namespace Klienci
{
    public partial class EdytujKlienta : Form
    {
        public int ID;
        private DataTable klient;
        private String regonStr;
        private String peselStr;
        private bool edited;
        private String polisaId;
        private String typId;
        private String klientId;
        private int opMode;
        private String rodzaj;
        private String przedmiotId;
        private String token;
        private bool nowaPolisa;

        public EdytujKlienta()
        {
            InitializeComponent();
            foreach (Control control in this.clientData.Controls)
            {
                if (control is TxtBox || control is Masked || control is RichTxt)
                {
                    control.KeyDown += new System.Windows.Forms.KeyEventHandler(this.control_KeyDown);
                }
            }

            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();
            this.dataOd.Value = this.dataZawarcia.Value.AddDays(1);
            this.dataDo.Value = this.dataOd.Value.AddYears(1).AddDays(-1);

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            this.token = new String(stringChars);
            foreach (Control ctrl in groupBox1.Controls)
            {
                if (ctrl is TxtBox || ctrl is Masked)
                {
                    ctrl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.control_KeyDown);
                }
            }
            foreach (Control ctrl in groupBox2.Controls)
            {
                if (ctrl is TxtBox || ctrl is Masked)
                {
                    ctrl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.control_KeyDown);
                }
            }
            this.polisaId = "";
            this.typId = "";
            this.przedmiotId = "";
        }

        private void EdytujKlienta_Load(object sender, EventArgs e)
        {
            this.klientId = this.ID.ToString();
            this.polisyTableAdapter1.Fill(this.databaseDataSet1.Polisy);
            this.prodKlientaTableAdapter1.Fill(this.databaseDataSet1.ProdKlienta);
            this.produktyTableAdapter1.Fill(this.databaseDataSet1.Produkty);
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();

                String strSQL = "SELECT * FROM Klienci WHERE Id_klienta='" + this.ID + "';";
                SqlDataAdapter da = new SqlDataAdapter(strSQL, conn);
                klient = new DataTable();
                da.Fill(klient);//<-there is an exception
                DataRow row = klient.Rows[0];
                this.nazwisko.Text = row["Nazwisko"].ToString();
                this.imie.Text = row["Imie"].ToString();
                this.firma.Text = row["Firma"].ToString();
                this.pesel.Text = row["PESEL"].ToString();
                this.regon.Text = row["REGON"].ToString();
                this.nr_tel.Text = row["Nr_tel"].ToString();
                this.email.Text = row["E_mail"].ToString();
                this.ulica.Text = row["Ulica"].ToString();
                this.miejscowosc.Text = row["Miejscowosc"].ToString();
                this.kod_pocztowy.Text = row["Kod_pocztowy"].ToString();
                this.uwagi.Text = row["Uwagi"].ToString();
                this.dodany.Text = row["Dodany"].ToString();
                this.edytowany.Text = row["Edytowany"].ToString();
                this.Text = row["Imie"].ToString() + " " + row["Nazwisko"].ToString();
                this.regonStr = row["REGON"].ToString();
                this.peselStr = this.pesel.Text = row["PESEL"].ToString();
                conn.Close();
            }

            DataTable polisy = this.databaseDataSet1.Polisy;
            DataTable prod = this.databaseDataSet1.ProdKlienta;
            DataTable typy = this.databaseDataSet1.Produkty;
            DataRow[] pol = polisy.Select("Id_klienta="+this.ID);
            this.treeView1.Nodes.Clear();
            int ijk = 1;
            foreach (DataRow row in pol)
            {
                DataRow[] row2 = prod.Select("Id_polisy=" + row["Id_polisy"]);
                if (row2.Length > 0 || row["Nazwa"].ToString().Equals(this.token))
                {
                    TreeNode parent = new TreeNode();
                    parent.ImageIndex = 0;
                    parent.Text = "Polisa " + ijk; //row["Nazwa"].ToString();
                    parent.Tag = row["Id_polisy"].ToString();
                    this.treeView1.Nodes.Add(parent);
                    ijk++;
                    foreach (DataRow produkt in row2)
                    {
                        DataRow[] typ = typy.Select("Id_produktu=" + produkt["Id_typu"]);
                        TreeNode child = new TreeNode();
                        child.Text = typ[0]["Rodzaj"].ToString();
                        child.ImageIndex = 1;
                        child.Tag = produkt["Id_prodKlienta"].ToString();
                        parent.Nodes.Add(child);
                    }
                }
            }
            treeView1.SelectedImageIndex = 2;

            foreach (DataRow row in typy.Rows)
            {
                List<string> itemStr = new List<string>();
                int i = 1;
                while (i < typy.Columns.Count)
                {
                    itemStr.Add(row[i] + "");
                    i++;
                }
                itemStr.Add(row[0] + "");
                ListViewItem item = new ListViewItem(itemStr.ToArray());
                this.typPolisy.Items.Add(item);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                String strSQL = "";
                if (pesel.Text != "")
                {
                    strSQL = "SELECT * FROM Klienci WHERE PESEL='" + pesel.Text + "';";
                }
                if (regon.Text != "")
                {
                    strSQL = "SELECT * FROM Klienci WHERE REGON='" + regon.Text + "';";
                }

                if (strSQL.Equals(""))
                {
                    MessageBox.Show("Proszę wypełnić pole REGON lub PESEL w zależności od typu klienta.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                SqlDataAdapter da = new SqlDataAdapter(strSQL, conn);
                DataTable x = new DataTable();
                da.Fill(x);
                conn.Close();
                if (x.Rows.Count > 0)
                {
                    String idType = "";
                    String peselDb = (String)x.Rows[0]["PESEL"];
                    String regonDb = (String)x.Rows[0]["REGON"];
                    if (!peselDb.Equals("           "))
                    {
                        idType = "PESEL";
                    }
                    else if (!regonDb.Equals("         "))
                    {
                        idType = "REGON";
                    }
                    MessageBox.Show("W bazie danych istnieje już klient o podanym numerze " + idType + ".", "Ostrzeżenie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    DialogResult r = MessageBox.Show("Czy na pewno chcesz zapisać zmiany dla tego klienta?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        Klient klient = new Klient();
                        klient.ID = this.ID;
                        klient.Nazwisko = nazwisko.Text;
                        klient.Imie = imie.Text;
                        klient.Firma = firma.Text;
                        klient.PESEL = pesel.Text;
                        klient.REGON = regon.Text;
                        klient.Nr_tel = nr_tel.Text;
                        klient.E_mail = email.Text;
                        klient.Ulica = ulica.Text;
                        klient.Miejscowosc = miejscowosc.Text;
                        klient.Kod_pocztowy = kod_pocztowy.Text;
                        klient.Uwagi = uwagi.Text;
                        klient.ZapiszKlienta();
                        this.edited = false;
                        //this.Close();
                    }
                }

            }
        }

        private void control_KeyDown(object sender, KeyEventArgs e)
        {
            this.edited = true;
        }

        private void EdytujKlienta_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.edited)
            {
                DialogResult r = MessageBox.Show("Zawartość niektórych elementów została zmieniona. Wprowadzone zmiany zostaną utracone.\nKontynuować?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    e.Cancel = false;
                    this.Hide();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void LoadDataToForm(String idPrzedmiotu)
        {
            this.przedmiotId = idPrzedmiotu;
            DataTable klienci = this.databaseDataSet1.Klienci;
            DataTable polisy = this.databaseDataSet1.Polisy;
            DataTable typy = this.databaseDataSet1.Produkty;
            DataTable przedmioty = this.databaseDataSet1.ProdKlienta;

            DataRow[] przedmiot = przedmioty.Select("Id_prodKlienta='" + idPrzedmiotu + "'");
            //DataRow[] polisa = polisy.Select("Id_polisy='" + this.polisaId + "'");
            //DataRow[] klient = klienci.Select("Id_klienta='" + polisa[0]["Id_klienta"].ToString() + "'");
           

            DataRow[] typ = typy.Select("Id_produktu='" + przedmiot[0]["Id_typu"].ToString() + "'");
            String txt = "";
            for (int i = 1; i < typy.Columns.Count - 1; i++)
            {
                txt += typ[0][i].ToString() + " ";
            }

            this.typId = przedmiot[0]["Id_typu"].ToString();

            this.txtTyp.Text = txt;
            this.prowizjaTxt.Text = typ[0]["Prowizja"].ToString();
            this.dataZawarcia.Value = DateTime.ParseExact(przedmiot[0]["Data_zawarcia"].ToString(), "dd-MM-yyyy", null); //Convert.ToDateTime(przedmiot[0]["Data_zawarcia"].ToString()); //DateTime.ParseExact(przedmiot[0]["Data_zawarcia"].ToString(), "dd-MM-yyy",null);
            this.dataOd.Value = DateTime.ParseExact(przedmiot[0]["Okres_od"].ToString(), "dd-MM-yyyy", null);
            this.dataDo.Value = DateTime.ParseExact(przedmiot[0]["Okres_do"].ToString(), "dd-MM-yyyy", null);
            this.skladka.Text = przedmiot[0]["Skladka"].ToString();
            this.suma.Text = przedmiot[0]["Suma_ubezpieczenia"].ToString();
            this.raty.Text = przedmiot[0]["Ilosc_rat"].ToString();
            this.uwagi.Text = przedmiot[0]["Uwagi"].ToString();
            String[] rows = przedmiot[0]["Raty"].ToString().Split('#');
            this.ratyGrid.Rows.Clear();
            foreach (String row in rows)
            {
                String[] cells = row.Split('|');
                this.ratyGrid.Rows.Add(cells);
            }
            if (przedmiot[0]["Status"].ToString().Equals("1"))
            {
                this.nowaRadio.Checked = true;
                this.wznowionaRadio.Checked = false;
            }
            else
            {
                this.nowaRadio.Checked = false;
                this.wznowionaRadio.Checked = true;
            }
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (this.edited && !this.nowaPolisa)
            {
                DialogResult r = MessageBox.Show("Zawartość niektórych pól została zmieniona. Wprowadzone zmiany zostaną utracone.\nKontynuować?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    
                }
                else
                {
                    return;
                }
            }
            if (e.Node.Parent != null)
            {
                this.opMode  =1;
                this.polisaId = e.Node.Parent.Tag.ToString();
                LoadDataToForm(e.Node.Tag.ToString());
            }
            else
            {
                this.polisaId = e.Node.Tag.ToString();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (this.typPolisy.Visible)
            {
                this.typPolisy.Visible = false;
            }
            else
            {
                this.typPolisy.Visible = true;
            }
        }

        private void typPolisy_Click(object sender, EventArgs e)
        {

        }

        private void typPolisy_DoubleClick(object sender, EventArgs e)
        {
            String typyStr = "";
            int i = 0;
            foreach (ListViewItem.ListViewSubItem item in this.typPolisy.SelectedItems[0].SubItems)
            {
                typyStr += item.Text + " ";
                i++;
                if (i == 3) break;
            }
            typId = this.typPolisy.SelectedItems[0].SubItems[this.typPolisy.SelectedItems[0].SubItems.Count - 1].Text + "";
            this.rodzaj = this.typPolisy.SelectedItems[0].SubItems[0].Text;
            this.prowizjaTxt.Text = this.typPolisy.SelectedItems[0].SubItems[1].Text;
            this.typPolisy.Visible = false;
            this.txtTyp.Text = typyStr;
        }

        private void zapiszBtn_Click(object sender, EventArgs e)
        {
            if (this.klientId == "")
            {
                MessageBox.Show("Proszę wybrać klienta.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (this.polisaId == "")
            {
                MessageBox.Show("Proszę wybrać polisę, do której ma zostać przypisany przedmiot.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (this.typId.Equals(""))
            {
                MessageBox.Show("Proszę wybrać typ ubezpieczenia.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            DialogResult r;
            if (this.opMode == 0)
            {
                r = MessageBox.Show("Czy chcesz dodać ten przedmiot do polisy?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            else
            {
                r = MessageBox.Show("Czy chcesz zapisać zmiany?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            if (r == DialogResult.Yes)
            {

            }
            else
            {
                return;
            }
            String raty = "";
            int j = 0;
            foreach (DataGridViewRow row in this.ratyGrid.Rows)
            {
                int i = 0;
                foreach (DataGridViewCell cell in row.Cells)
                {
                    raty += cell.Value + "";
                    i++;
                    if (i != row.Cells.Count)
                    {
                        raty += "|";
                    }
                }
                j++;
                if (j != ratyGrid.Rows.Count)
                {
                    raty += "#";
                }
            }
            ProdKlienta prod = new ProdKlienta();
            if (this.polisaId == "")
            {
                Polisa polisa = new Polisa();
                polisa.Id_klienta = this.klientId;
                polisa.Nazwa = this.rodzaj + " " + this.klient;
                polisa.DodajPolise();

                DataTable polisaOst;
                using (SqlConnection conn = new SqlConnection(Registry.ConnString))
                {
                    conn.Open();

                    String strSQL2 = "SELECT * FROM Polisy;";
                    SqlDataAdapter da2 = new SqlDataAdapter(strSQL2, conn);
                    polisaOst = new DataTable();
                    da2.Fill(polisaOst);//<-there is an exception

                    conn.Close();
                }
                polisaOst.DefaultView.Sort = "Id_polisy DESC";
                polisaOst = polisaOst.DefaultView.ToTable();
                prod.Id_polisy = polisaOst.Rows[0][0].ToString();
                this.polisaId = polisaOst.Rows[0][0].ToString();
            }
            else
            {
                prod.Id_polisy = this.polisaId;
            }
            prod.Id_typu = this.typId;
            prod.Okres_od = this.dataOd.Text;
            prod.Okres_do = this.dataDo.Text;
            prod.Suma_ubezpieczenia = this.suma.Text;
            prod.Skladka = this.skladka.Text;
            prod.Ilosc_rat = this.raty.Text;
            prod.Raty = raty;
            prod.Data_zawarcia = this.dataZawarcia.Text;
            prod.Uwagi = this.uwagi.Text;
            if (this.nowaRadio.Checked)
            {
                prod.Status = "1";
            }
            else
            {
                prod.Status = "0";
            }
            if (this.opMode == 0)
            {
                prod.DodajProdKlienta();
                this.prodKlientaTableAdapter1.Fill(this.databaseDataSet1.ProdKlienta);
                this.polisyTableAdapter1.Fill(this.databaseDataSet1.Polisy);
                //RefreshItems();
                //this.txtSearch.Enabled = false;
                EdytujKlienta_Load(null, null);
            }
            else
            {
                prod.EdytujProdKlienta(this.przedmiotId);
                EdytujKlienta_Load(null, null);
                //PolisyForm_Load(null, null);
            }
            this.edited = false;
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (this.edited && !nowaPolisa)
            {
                DialogResult r = MessageBox.Show("Zawartość niektórych pól została zmieniona. Wprowadzone zmiany zostaną utracone.\nKontynuować?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    this.edited = false;
                }
                else
                {
                    return;
                }
            }
            //this.txtSearch.Text = "";
            foreach (Control ctrl in groupBox1.Controls)
            {
                if (ctrl is TxtBox || ctrl is Masked)
                {
                    ctrl.Text = "";
                }
            }
            foreach (Control ctrl in groupBox2.Controls)
            {
                if (ctrl is TxtBox || ctrl is Masked)
                {
                    ctrl.Text = "";
                }
            }
            //this.polisaId = "";
            ratyGrid.Rows.Clear();
            this.opMode = 0;
            this.zapiszBtn.Text = "Dodaj";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.ratyGrid.Rows.Clear();
            String skladkaStr = this.skladka.Text;
            Decimal skladka = 0;
            if (skladkaStr != "")
            {
                skladka = Decimal.Parse(skladkaStr, CultureInfo.CurrentCulture);
            }
            else
            {
                MessageBox.Show("Proszę podać składkę ubezpieczenia.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int iloscRat = 0;
            if (this.raty.Text != "")
            {
                try
                {
                    iloscRat = int.Parse(this.raty.Text);
                }
                catch (Exception ee)
                {
                    MessageBox.Show("Proszę podać liczbę całkowitą w polu ilość rat.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Proszę podać ilość rat.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Decimal rata = skladka / iloscRat;
            Decimal.Round(rata, 2, MidpointRounding.ToEven);
            int daty = 365 / iloscRat;
            DateTime data = DateTime.Now;
            for (int i = 0; i < iloscRat; i++)
            {
                this.ratyGrid.Rows.Add(rata.ToString("N2"), data.AddDays(i * daty).ToString("dd-MM-yyyy"), false);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.nowaPolisa = true;
            this.edited = true;
            Polisa polisa = new Polisa();
            polisa.Id_klienta = this.klientId;
            polisa.Nazwa = this.token;
            polisa.DodajPolise();
            EdytujKlienta_Load(null, null);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (this.treeView1.SelectedNode.Parent == null)
            {
                DialogResult r = MessageBox.Show("Czy chcesz usunąć całą polisę wraz z przypisanymi do niej przedmiotami?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    foreach(TreeNode node in this.treeView1.SelectedNode.Nodes)
                    {
                        MessageBox.Show(node.Tag.ToString());
                        ProdKlienta.UsunProdKlienta(node.Tag.ToString());
                    }
                    Polisa.UsunPolise(this.treeView1.SelectedNode.Tag.ToString());
                    EdytujKlienta_Load(null, null);
                    foreach (Control ctrl in groupBox1.Controls)
                    {
                        if (ctrl is TxtBox || ctrl is Masked)
                        {
                            ctrl.Text = "";
                        }
                    }
                    foreach (Control ctrl in groupBox2.Controls)
                    {
                        if (ctrl is TxtBox || ctrl is Masked)
                        {
                            ctrl.Text = "";
                        }
                    }
                    //this.polisaId = "";
                    ratyGrid.Rows.Clear();
                    this.opMode = 0;
                    this.zapiszBtn.Text = "Dodaj";
                }
                else
                {
                    return;
                }
            }
            else
            {
                DialogResult r = MessageBox.Show("Czy chcesz usunąć przedmiot \"" + this.treeView1.SelectedNode.Text + "\" z polisy?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    ProdKlienta.UsunProdKlienta(this.treeView1.SelectedNode.Tag.ToString());
                    EdytujKlienta_Load(null,null);
                    foreach (Control ctrl in groupBox1.Controls)
                    {
                        if (ctrl is TxtBox || ctrl is Masked)
                        {
                            ctrl.Text = "";
                        }
                    }
                    foreach (Control ctrl in groupBox2.Controls)
                    {
                        if (ctrl is TxtBox || ctrl is Masked)
                        {
                            ctrl.Text = "";
                        }
                    }
                    //this.polisaId = "";
                    ratyGrid.Rows.Clear();
                    this.opMode = 0;
                    this.zapiszBtn.Text = "Dodaj";
                }
                else
                {
                    return;
                }
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (this.edited)
            {
                DialogResult r = MessageBox.Show("Zawartość niektórych pól została zmieniona. Wprowadzone zmiany zostaną utracone.\nKontynuować?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    this.edited = false;
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButton1.Checked)
            {
                this.panel2.Visible = false;
                this.panel1.Visible = true;
            }
            else
            {
                this.panel1.Visible = false;
                this.panel2.Visible = true;
            }
        }
    }
}
