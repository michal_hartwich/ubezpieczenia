﻿namespace Klienci
{
    partial class DodajKlienta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.clientMode = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.clientData = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.regon = new Controls.Masked();
            this.label4 = new System.Windows.Forms.Label();
            this.firma = new Controls.TxtBox();
            this.label3 = new System.Windows.Forms.Label();
            this.uwagi = new Controls.RichTxt();
            this.label11 = new System.Windows.Forms.Label();
            this.kod_pocztowy = new Controls.Masked();
            this.label10 = new System.Windows.Forms.Label();
            this.miejscowosc = new Controls.TxtBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ulica = new Controls.TxtBox();
            this.label8 = new System.Windows.Forms.Label();
            this.email = new Controls.Masked();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pesel = new Controls.Masked();
            this.label6 = new System.Windows.Forms.Label();
            this.nr_tel = new Controls.TxtBox();
            this.label5 = new System.Windows.Forms.Label();
            this.imie = new Controls.TxtBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nazwisko = new Controls.TxtBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dontHide = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.clientMode.SuspendLayout();
            this.clientData.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 508F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.clientMode, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.clientData, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.21053F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85.78947F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(508, 412);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // clientMode
            // 
            this.clientMode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.clientMode.Controls.Add(this.radioButton2);
            this.clientMode.Controls.Add(this.radioButton1);
            this.clientMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientMode.Location = new System.Drawing.Point(3, 3);
            this.clientMode.Name = "clientMode";
            this.clientMode.Size = new System.Drawing.Size(502, 46);
            this.clientMode.TabIndex = 1;
            this.clientMode.TabStop = false;
            this.clientMode.Text = "Charakter działaności";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(278, 19);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(102, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Osoba prywatna";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(141, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(50, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Firma";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // clientData
            // 
            this.clientData.Controls.Add(this.panel1);
            this.clientData.Controls.Add(this.uwagi);
            this.clientData.Controls.Add(this.label11);
            this.clientData.Controls.Add(this.kod_pocztowy);
            this.clientData.Controls.Add(this.label10);
            this.clientData.Controls.Add(this.miejscowosc);
            this.clientData.Controls.Add(this.label9);
            this.clientData.Controls.Add(this.ulica);
            this.clientData.Controls.Add(this.label8);
            this.clientData.Controls.Add(this.email);
            this.clientData.Controls.Add(this.label7);
            this.clientData.Controls.Add(this.panel2);
            this.clientData.Controls.Add(this.nr_tel);
            this.clientData.Controls.Add(this.label5);
            this.clientData.Controls.Add(this.imie);
            this.clientData.Controls.Add(this.label2);
            this.clientData.Controls.Add(this.nazwisko);
            this.clientData.Controls.Add(this.label1);
            this.clientData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientData.Location = new System.Drawing.Point(3, 55);
            this.clientData.Name = "clientData";
            this.clientData.Size = new System.Drawing.Size(502, 310);
            this.clientData.TabIndex = 2;
            this.clientData.TabStop = false;
            this.clientData.Text = "Dane klienta";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.regon);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.firma);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(9, 52);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.panel1.Size = new System.Drawing.Size(481, 27);
            this.panel1.TabIndex = 4;
            // 
            // regon
            // 
            this.regon.Edited = false;
            this.regon.Location = new System.Drawing.Point(356, 3);
            this.regon.Mask = "000000000";
            this.regon.Name = "regon";
            this.regon.Size = new System.Drawing.Size(125, 20);
            this.regon.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(301, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "REGON:";
            // 
            // firma
            // 
            this.firma.Edited = false;
            this.firma.Location = new System.Drawing.Point(62, 3);
            this.firma.Name = "firma";
            this.firma.Size = new System.Drawing.Size(233, 20);
            this.firma.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Firma:";
            // 
            // uwagi
            // 
            this.uwagi.Edited = false;
            this.uwagi.Location = new System.Drawing.Point(12, 179);
            this.uwagi.Name = "uwagi";
            this.uwagi.Size = new System.Drawing.Size(478, 125);
            this.uwagi.TabIndex = 16;
            this.uwagi.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 163);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Informacje dodatkowe";
            // 
            // kod_pocztowy
            // 
            this.kod_pocztowy.Edited = false;
            this.kod_pocztowy.Location = new System.Drawing.Point(400, 134);
            this.kod_pocztowy.Mask = "00-000";
            this.kod_pocztowy.Name = "kod_pocztowy";
            this.kod_pocztowy.Size = new System.Drawing.Size(90, 20);
            this.kod_pocztowy.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(320, 137);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Kod pocztowy";
            // 
            // miejscowosc
            // 
            this.miejscowosc.Edited = false;
            this.miejscowosc.Location = new System.Drawing.Point(95, 134);
            this.miejscowosc.Name = "miejscowosc";
            this.miejscowosc.Size = new System.Drawing.Size(209, 20);
            this.miejscowosc.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 137);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Miejscowość:";
            // 
            // ulica
            // 
            this.ulica.Edited = false;
            this.ulica.Location = new System.Drawing.Point(95, 108);
            this.ulica.Name = "ulica";
            this.ulica.Size = new System.Drawing.Size(395, 20);
            this.ulica.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 111);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Ulica i nr domu:";
            // 
            // email
            // 
            this.email.Edited = false;
            this.email.Location = new System.Drawing.Point(310, 82);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(180, 20);
            this.email.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(266, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "E-mail:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pesel);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(9, 52);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.panel2.Size = new System.Drawing.Size(481, 27);
            this.panel2.TabIndex = 5;
            this.panel2.Visible = false;
            // 
            // pesel
            // 
            this.pesel.Edited = false;
            this.pesel.Location = new System.Drawing.Point(62, 3);
            this.pesel.Mask = "00000000000";
            this.pesel.Name = "pesel";
            this.pesel.Size = new System.Drawing.Size(180, 20);
            this.pesel.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "PESEL:";
            // 
            // nr_tel
            // 
            this.nr_tel.Edited = false;
            this.nr_tel.Location = new System.Drawing.Point(71, 82);
            this.nr_tel.Name = "nr_tel";
            this.nr_tel.Size = new System.Drawing.Size(180, 20);
            this.nr_tel.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Nr telefonu:";
            // 
            // imie
            // 
            this.imie.Edited = false;
            this.imie.Location = new System.Drawing.Point(310, 29);
            this.imie.Name = "imie";
            this.imie.Size = new System.Drawing.Size(180, 20);
            this.imie.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(275, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Imię:";
            // 
            // nazwisko
            // 
            this.nazwisko.Edited = false;
            this.nazwisko.Location = new System.Drawing.Point(71, 29);
            this.nazwisko.Name = "nazwisko";
            this.nazwisko.Size = new System.Drawing.Size(180, 20);
            this.nazwisko.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nazwisko:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.2F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.8F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 126F));
            this.tableLayoutPanel2.Controls.Add(this.button1, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.button2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.dontHide, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 371);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(502, 38);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Location = new System.Drawing.Point(379, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 32);
            this.button1.TabIndex = 5;
            this.button1.Text = "Dodaj klienta";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Dock = System.Windows.Forms.DockStyle.Right;
            this.button2.Location = new System.Drawing.Point(255, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 32);
            this.button2.TabIndex = 6;
            this.button2.Text = "Anuluj";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // dontHide
            // 
            this.dontHide.AutoSize = true;
            this.dontHide.Dock = System.Windows.Forms.DockStyle.Left;
            this.dontHide.Location = new System.Drawing.Point(3, 3);
            this.dontHide.Name = "dontHide";
            this.dontHide.Size = new System.Drawing.Size(190, 32);
            this.dontHide.TabIndex = 7;
            this.dontHide.Text = "Nie zamykaj tego okna po dodaniu";
            this.dontHide.UseVisualStyleBackColor = true;
            // 
            // DodajKlienta
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(508, 412);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DodajKlienta";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dodawanie nowego klienta";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DodajKlienta_FormClosing);
            this.Load += new System.EventHandler(this.DodajKlienta_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.clientMode.ResumeLayout(false);
            this.clientMode.PerformLayout();
            this.clientData.ResumeLayout(false);
            this.clientData.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox clientMode;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox clientData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Controls.Masked pesel;
        private System.Windows.Forms.Label label6;
        private Controls.Masked regon;
        private System.Windows.Forms.Label label4;
        private Controls.TxtBox firma;
        private System.Windows.Forms.Label label3;
        private Controls.TxtBox imie;
        private System.Windows.Forms.Label label2;
        private Controls.TxtBox nazwisko;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private Controls.TxtBox miejscowosc;
        private System.Windows.Forms.Label label9;
        private Controls.TxtBox ulica;
        private System.Windows.Forms.Label label8;
        private Controls.Masked email;
        private System.Windows.Forms.Label label7;
        private Controls.TxtBox nr_tel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private Controls.Masked kod_pocztowy;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private Controls.RichTxt uwagi;
        private System.Windows.Forms.CheckBox dontHide;

    }
}