﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using StaticRegistry;
using Controls;

namespace Klienci
{
    public partial class DodajKlienta : Form
    {
        private bool edited = false;

        public DodajKlienta()
        {
            InitializeComponent();
            foreach (Control control in this.clientData.Controls)
            {
                if (control is TxtBox || control is Masked || control is RichTxt)
                {
                    control.KeyDown += new System.Windows.Forms.KeyEventHandler(this.control_KeyDown);
                }
            }
        }

        private void DodajKlienta_Load(object sender, EventArgs e)
        {
            if (this.radioButton1.Checked)
            {
                this.panel2.Visible = false;
                this.panel1.Visible = true;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButton1.Checked)
            {
                this.panel2.Visible = false;
                this.panel1.Visible = true;
            }
            else
            {
                this.panel1.Visible = false;
                this.panel2.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
               
                 String strSQL = "";
                if (pesel.Text != "")
                {
                    strSQL = "SELECT * FROM Klienci WHERE PESEL='" + pesel.Text + "';";
                }
                if (regon.Text != "")
                {
                    strSQL = "SELECT * FROM Klienci WHERE REGON='" + regon.Text + "';";
                }
         
                if (strSQL.Equals(""))
                {
                    MessageBox.Show("Proszę wypełnić pole REGON lub PESEL w zależności od typu klienta.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                SqlDataAdapter da = new SqlDataAdapter(strSQL, conn);
                DataTable x = new DataTable();
                da.Fill(x);
                conn.Close();
                if(x.Rows.Count > 0)
                {
                    String idType="";
                    String peselDb = (String) x.Rows[0]["PESEL"];
                    String regonDb = (String)x.Rows[0]["REGON"];
                    
                    if (!peselDb.Equals("           "))
                    {
                        idType = "PESEL";
                    }
                    else if (!regonDb.Equals("         "))
                    {
                        idType = "REGON";
                    }
                    MessageBox.Show("W bazie danych istnieje już klient o podanym numerze " + idType + ".", "Ostrzeżenie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    DialogResult r = MessageBox.Show("Czy na pewno chcesz dodać tego klienta?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        Klient klient = new Klient();
                        klient.Nazwisko = nazwisko.Text;
                        klient.Imie = imie.Text;
                        klient.Firma = firma.Text;
                        klient.PESEL = pesel.Text;
                        klient.REGON = regon.Text;
                        klient.Nr_tel = nr_tel.Text;
                        klient.E_mail = email.Text;
                        klient.Ulica = ulica.Text;
                        klient.Miejscowosc = miejscowosc.Text;
                        klient.Kod_pocztowy = kod_pocztowy.Text;
                        klient.Uwagi = uwagi.Text;
                        klient.DodajKlienta();
                        this.edited = false;
                        if (dontHide.Checked)
                        {
                            foreach (Control control in this.clientData.Controls)
                            {
                                if (control is TxtBox || control is Masked || control is RichTxt)
                                {
                                    control.Text = "";
                                }

                            }
                            this.firma.Text = "";
                            this.regon.Text = "";
                            this.pesel.Text = "";
                        }
                        else
                        {
                            this.Close();
                        }
                    }
                }
                
            }
            
        }

        private void DodajKlienta_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.edited)
            {
                DialogResult r = MessageBox.Show("Zawartość niektórych elementów została zmieniona. Wprowadzone zmiany zostaną utracone.\nKontynuować?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    e.Cancel = false;
                    this.Hide();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void control_KeyDown(object sender, KeyEventArgs e)
        {
            this.edited = true;
        }
    }
}
