﻿namespace Klienci
{
    partial class EdytujKlienta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EdytujKlienta));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.anulujBtn = new System.Windows.Forms.Button();
            this.zapiszBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.typPolisy = new System.Windows.Forms.ListView();
            this.Rodzaj = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Prowizja = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Opis = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.richTxt1 = new Controls.RichTxt();
            this.label14 = new System.Windows.Forms.Label();
            this.wznowionaRadio = new System.Windows.Forms.RadioButton();
            this.nowaRadio = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.prowizjaTxt = new Controls.TxtBox();
            this.label16 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ratyGrid = new System.Windows.Forms.DataGridView();
            this.wys_raty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.term_zaplaty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zaplacono = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label17 = new System.Windows.Forms.Label();
            this.skladka = new Controls.TxtBox();
            this.label18 = new System.Windows.Forms.Label();
            this.raty = new Controls.TxtBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.suma = new Controls.TxtBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.dataDo = new System.Windows.Forms.DateTimePicker();
            this.dataOd = new System.Windows.Forms.DateTimePicker();
            this.label24 = new System.Windows.Forms.Label();
            this.dataZawarcia = new System.Windows.Forms.DateTimePicker();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.txtTyp = new Controls.TxtBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.clientMode = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.clientData = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.edytowany = new Controls.TxtBox();
            this.dodany = new Controls.TxtBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.regon = new Controls.Masked();
            this.label4 = new System.Windows.Forms.Label();
            this.firma = new Controls.TxtBox();
            this.label3 = new System.Windows.Forms.Label();
            this.uwagi = new Controls.RichTxt();
            this.label11 = new System.Windows.Forms.Label();
            this.kod_pocztowy = new Controls.Masked();
            this.label10 = new System.Windows.Forms.Label();
            this.miejscowosc = new Controls.TxtBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ulica = new Controls.TxtBox();
            this.label8 = new System.Windows.Forms.Label();
            this.email = new Controls.Masked();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pesel = new Controls.Masked();
            this.label6 = new System.Windows.Forms.Label();
            this.nr_tel = new Controls.TxtBox();
            this.label5 = new System.Windows.Forms.Label();
            this.imie = new Controls.TxtBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nazwisko = new Controls.TxtBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.button7 = new System.Windows.Forms.Button();
            this.databaseDataSet1 = new Elements.databaseDataSet();
            this.polisyTableAdapter1 = new Elements.databaseDataSetTableAdapters.PolisyTableAdapter();
            this.prodKlientaTableAdapter1 = new Elements.databaseDataSetTableAdapters.ProdKlientaTableAdapter();
            this.produktyTableAdapter1 = new Elements.databaseDataSetTableAdapters.ProduktyTableAdapter();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ratyGrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.clientMode.SuspendLayout();
            this.clientData.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(5, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(642, 491);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.LightBlue;
            this.tabPage1.Controls.Add(this.tableLayoutPanel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(634, 465);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Polisy klienta";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.80706F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.19295F));
            this.tableLayoutPanel3.Controls.Add(this.panel3, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(628, 459);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.anulujBtn);
            this.panel3.Controls.Add(this.zapiszBtn);
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(157, 2);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(469, 455);
            this.panel3.TabIndex = 2;
            // 
            // anulujBtn
            // 
            this.anulujBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.anulujBtn.Location = new System.Drawing.Point(281, 420);
            this.anulujBtn.Margin = new System.Windows.Forms.Padding(2);
            this.anulujBtn.Name = "anulujBtn";
            this.anulujBtn.Size = new System.Drawing.Size(91, 32);
            this.anulujBtn.TabIndex = 3;
            this.anulujBtn.Text = "Zamknij";
            this.anulujBtn.UseVisualStyleBackColor = true;
            // 
            // zapiszBtn
            // 
            this.zapiszBtn.Location = new System.Drawing.Point(376, 420);
            this.zapiszBtn.Margin = new System.Windows.Forms.Padding(2);
            this.zapiszBtn.Name = "zapiszBtn";
            this.zapiszBtn.Size = new System.Drawing.Size(91, 32);
            this.zapiszBtn.TabIndex = 14;
            this.zapiszBtn.Text = "Zapisz";
            this.zapiszBtn.UseVisualStyleBackColor = true;
            this.zapiszBtn.Click += new System.EventHandler(this.zapiszBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.typPolisy);
            this.groupBox2.Controls.Add(this.richTxt1);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.wznowionaRadio);
            this.groupBox2.Controls.Add(this.nowaRadio);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.prowizjaTxt);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.skladka);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.raty);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.suma);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.dataDo);
            this.groupBox2.Controls.Add(this.dataOd);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.dataZawarcia);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Location = new System.Drawing.Point(2, 54);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(464, 362);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Szczególy";
            // 
            // typPolisy
            // 
            this.typPolisy.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Rodzaj,
            this.Prowizja,
            this.Opis});
            this.typPolisy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.typPolisy.FullRowSelect = true;
            this.typPolisy.GridLines = true;
            this.typPolisy.Location = new System.Drawing.Point(170, 0);
            this.typPolisy.Margin = new System.Windows.Forms.Padding(2);
            this.typPolisy.MultiSelect = false;
            this.typPolisy.Name = "typPolisy";
            this.typPolisy.Size = new System.Drawing.Size(259, 140);
            this.typPolisy.TabIndex = 1;
            this.typPolisy.UseCompatibleStateImageBehavior = false;
            this.typPolisy.View = System.Windows.Forms.View.Details;
            this.typPolisy.Visible = false;
            this.typPolisy.Click += new System.EventHandler(this.typPolisy_Click);
            this.typPolisy.DoubleClick += new System.EventHandler(this.typPolisy_DoubleClick);
            // 
            // Rodzaj
            // 
            this.Rodzaj.Text = "Rodzaj";
            this.Rodzaj.Width = 85;
            // 
            // Prowizja
            // 
            this.Prowizja.Text = "Prowizja";
            this.Prowizja.Width = 85;
            // 
            // Opis
            // 
            this.Opis.Text = "Opis";
            this.Opis.Width = 212;
            // 
            // richTxt1
            // 
            this.richTxt1.Edited = false;
            this.richTxt1.Location = new System.Drawing.Point(5, 292);
            this.richTxt1.Margin = new System.Windows.Forms.Padding(2);
            this.richTxt1.Name = "richTxt1";
            this.richTxt1.Size = new System.Drawing.Size(453, 66);
            this.richTxt1.TabIndex = 13;
            this.richTxt1.Text = "";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 276);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Uwagi:";
            // 
            // wznowionaRadio
            // 
            this.wznowionaRadio.AutoSize = true;
            this.wznowionaRadio.Checked = true;
            this.wznowionaRadio.Location = new System.Drawing.Point(242, 255);
            this.wznowionaRadio.Margin = new System.Windows.Forms.Padding(2);
            this.wznowionaRadio.Name = "wznowionaRadio";
            this.wznowionaRadio.Size = new System.Drawing.Size(81, 17);
            this.wznowionaRadio.TabIndex = 12;
            this.wznowionaRadio.TabStop = true;
            this.wznowionaRadio.Text = "Wznowiona";
            this.wznowionaRadio.UseVisualStyleBackColor = true;
            // 
            // nowaRadio
            // 
            this.nowaRadio.AutoSize = true;
            this.nowaRadio.Location = new System.Drawing.Point(140, 255);
            this.nowaRadio.Margin = new System.Windows.Forms.Padding(2);
            this.nowaRadio.Name = "nowaRadio";
            this.nowaRadio.Size = new System.Drawing.Size(53, 17);
            this.nowaRadio.TabIndex = 11;
            this.nowaRadio.Text = "Nowa";
            this.nowaRadio.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 257);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "Status:";
            // 
            // prowizjaTxt
            // 
            this.prowizjaTxt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.prowizjaTxt.Edited = false;
            this.prowizjaTxt.Enabled = false;
            this.prowizjaTxt.Location = new System.Drawing.Point(307, 57);
            this.prowizjaTxt.Margin = new System.Windows.Forms.Padding(2);
            this.prowizjaTxt.Name = "prowizjaTxt";
            this.prowizjaTxt.ReadOnly = true;
            this.prowizjaTxt.Size = new System.Drawing.Size(57, 20);
            this.prowizjaTxt.TabIndex = 18;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(260, 59);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "Prowizja:";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button3.Location = new System.Drawing.Point(370, 75);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(89, 27);
            this.button3.TabIndex = 9;
            this.button3.Text = "Oblicz raty";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ratyGrid);
            this.groupBox3.Location = new System.Drawing.Point(4, 100);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(456, 151);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Raty";
            // 
            // ratyGrid
            // 
            this.ratyGrid.AllowUserToAddRows = false;
            this.ratyGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ratyGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.ratyGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ratyGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.wys_raty,
            this.term_zaplaty,
            this.zaplacono});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ratyGrid.DefaultCellStyle = dataGridViewCellStyle10;
            this.ratyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ratyGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.ratyGrid.Location = new System.Drawing.Point(2, 15);
            this.ratyGrid.Margin = new System.Windows.Forms.Padding(2);
            this.ratyGrid.Name = "ratyGrid";
            this.ratyGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ratyGrid.Size = new System.Drawing.Size(452, 134);
            this.ratyGrid.TabIndex = 10;
            // 
            // wys_raty
            // 
            this.wys_raty.HeaderText = "Wysokość raty";
            this.wys_raty.Name = "wys_raty";
            this.wys_raty.Width = 150;
            // 
            // term_zaplaty
            // 
            this.term_zaplaty.HeaderText = "Termin zapłaty";
            this.term_zaplaty.Name = "term_zaplaty";
            this.term_zaplaty.Width = 150;
            // 
            // zaplacono
            // 
            this.zaplacono.FalseValue = "0";
            this.zaplacono.HeaderText = "Zapłacono";
            this.zaplacono.Name = "zaplacono";
            this.zaplacono.TrueValue = "1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(239, 81);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "zł";
            // 
            // skladka
            // 
            this.skladka.Edited = false;
            this.skladka.Location = new System.Drawing.Point(139, 79);
            this.skladka.Margin = new System.Windows.Forms.Padding(2);
            this.skladka.Name = "skladka";
            this.skladka.Size = new System.Drawing.Size(95, 20);
            this.skladka.TabIndex = 7;
            this.skladka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 81);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(51, 13);
            this.label18.TabIndex = 12;
            this.label18.Text = "Składka:";
            // 
            // raty
            // 
            this.raty.Edited = false;
            this.raty.Location = new System.Drawing.Point(307, 79);
            this.raty.Margin = new System.Windows.Forms.Padding(2);
            this.raty.Name = "raty";
            this.raty.Size = new System.Drawing.Size(57, 20);
            this.raty.TabIndex = 8;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(260, 81);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 13);
            this.label19.TabIndex = 10;
            this.label19.Text = "Ilość rat:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(239, 59);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 13);
            this.label20.TabIndex = 9;
            this.label20.Text = "zł";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(5, 59);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(108, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "Suma ubezpieczenia:";
            // 
            // suma
            // 
            this.suma.Edited = false;
            this.suma.Location = new System.Drawing.Point(139, 57);
            this.suma.Margin = new System.Windows.Forms.Padding(2);
            this.suma.Name = "suma";
            this.suma.Size = new System.Drawing.Size(95, 20);
            this.suma.TabIndex = 6;
            this.suma.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(239, 36);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(22, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "do:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(116, 36);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(22, 13);
            this.label23.TabIndex = 5;
            this.label23.Text = "od:";
            // 
            // dataDo
            // 
            this.dataDo.CustomFormat = "dd-MM-yyyy";
            this.dataDo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataDo.Location = new System.Drawing.Point(262, 35);
            this.dataDo.Margin = new System.Windows.Forms.Padding(2);
            this.dataDo.Name = "dataDo";
            this.dataDo.Size = new System.Drawing.Size(102, 20);
            this.dataDo.TabIndex = 5;
            // 
            // dataOd
            // 
            this.dataOd.CustomFormat = "dd-MM-yyyy";
            this.dataOd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataOd.Location = new System.Drawing.Point(139, 35);
            this.dataOd.Margin = new System.Windows.Forms.Padding(2);
            this.dataOd.Name = "dataOd";
            this.dataOd.Size = new System.Drawing.Size(95, 20);
            this.dataOd.TabIndex = 4;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(5, 36);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(109, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Okres ubezpieczenia:";
            // 
            // dataZawarcia
            // 
            this.dataZawarcia.CustomFormat = "dd-MM-yyyy";
            this.dataZawarcia.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZawarcia.Location = new System.Drawing.Point(139, 14);
            this.dataZawarcia.Margin = new System.Windows.Forms.Padding(2);
            this.dataZawarcia.Name = "dataZawarcia";
            this.dataZawarcia.Size = new System.Drawing.Size(95, 20);
            this.dataZawarcia.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(4, 14);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Data zawarcia:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.txtTyp);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(464, 48);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Typ ubezpieczenia";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(433, 17);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(27, 21);
            this.button4.TabIndex = 2;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtTyp
            // 
            this.txtTyp.Edited = false;
            this.txtTyp.Location = new System.Drawing.Point(170, 18);
            this.txtTyp.Margin = new System.Windows.Forms.Padding(2);
            this.txtTyp.Name = "txtTyp";
            this.txtTyp.Size = new System.Drawing.Size(259, 20);
            this.txtTyp.TabIndex = 2;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label26.Location = new System.Drawing.Point(4, 21);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(162, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Wybierz typ ubezpieczenia:";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBox4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(2, 2);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.panel4.Size = new System.Drawing.Size(151, 455);
            this.panel4.TabIndex = 3;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button7);
            this.groupBox4.Controls.Add(this.treeView1);
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.button6);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 2);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(151, 453);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Polisy klienta";
            // 
            // treeView1
            // 
            this.treeView1.HideSelection = false;
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(3, 19);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.Size = new System.Drawing.Size(142, 356);
            this.treeView1.TabIndex = 5;
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1, 418);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(76, 32);
            this.button5.TabIndex = 4;
            this.button5.Text = "Usuń";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(71, 418);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(78, 32);
            this.button6.TabIndex = 3;
            this.button6.Text = "Dodaj";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LightBlue;
            this.tabPage2.Controls.Add(this.tableLayoutPanel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(634, 465);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Dane klienta";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 628F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.clientMode, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.clientData, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.21053F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85.78947F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(628, 459);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // clientMode
            // 
            this.clientMode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.clientMode.Controls.Add(this.radioButton2);
            this.clientMode.Controls.Add(this.radioButton1);
            this.clientMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientMode.Location = new System.Drawing.Point(3, 3);
            this.clientMode.Name = "clientMode";
            this.clientMode.Size = new System.Drawing.Size(622, 52);
            this.clientMode.TabIndex = 1;
            this.clientMode.TabStop = false;
            this.clientMode.Text = "Charakter działaności";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(345, 19);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(102, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Osoba prywatna";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(208, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(50, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Firma";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // clientData
            // 
            this.clientData.Controls.Add(this.label13);
            this.clientData.Controls.Add(this.edytowany);
            this.clientData.Controls.Add(this.dodany);
            this.clientData.Controls.Add(this.label12);
            this.clientData.Controls.Add(this.panel1);
            this.clientData.Controls.Add(this.uwagi);
            this.clientData.Controls.Add(this.label11);
            this.clientData.Controls.Add(this.kod_pocztowy);
            this.clientData.Controls.Add(this.label10);
            this.clientData.Controls.Add(this.miejscowosc);
            this.clientData.Controls.Add(this.label9);
            this.clientData.Controls.Add(this.ulica);
            this.clientData.Controls.Add(this.label8);
            this.clientData.Controls.Add(this.email);
            this.clientData.Controls.Add(this.label7);
            this.clientData.Controls.Add(this.panel2);
            this.clientData.Controls.Add(this.nr_tel);
            this.clientData.Controls.Add(this.label5);
            this.clientData.Controls.Add(this.imie);
            this.clientData.Controls.Add(this.label2);
            this.clientData.Controls.Add(this.nazwisko);
            this.clientData.Controls.Add(this.label1);
            this.clientData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientData.Location = new System.Drawing.Point(3, 61);
            this.clientData.Name = "clientData";
            this.clientData.Size = new System.Drawing.Size(622, 348);
            this.clientData.TabIndex = 2;
            this.clientData.TabStop = false;
            this.clientData.Text = "Dane klienta";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(165, 307);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "Edytowany:";
            // 
            // edytowany
            // 
            this.edytowany.Edited = false;
            this.edytowany.Enabled = false;
            this.edytowany.Location = new System.Drawing.Point(168, 323);
            this.edytowany.Name = "edytowany";
            this.edytowany.ReadOnly = true;
            this.edytowany.Size = new System.Drawing.Size(150, 20);
            this.edytowany.TabIndex = 19;
            // 
            // dodany
            // 
            this.dodany.Edited = false;
            this.dodany.Enabled = false;
            this.dodany.Location = new System.Drawing.Point(12, 323);
            this.dodany.Name = "dodany";
            this.dodany.ReadOnly = true;
            this.dodany.Size = new System.Drawing.Size(150, 20);
            this.dodany.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 307);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "Dodany:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.regon);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.firma);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(27, 51);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.panel1.Size = new System.Drawing.Size(602, 27);
            this.panel1.TabIndex = 4;
            // 
            // regon
            // 
            this.regon.Edited = false;
            this.regon.Location = new System.Drawing.Point(391, 3);
            this.regon.Mask = "000000000";
            this.regon.Name = "regon";
            this.regon.Size = new System.Drawing.Size(180, 20);
            this.regon.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(336, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "REGON:";
            // 
            // firma
            // 
            this.firma.Edited = false;
            this.firma.Location = new System.Drawing.Point(62, 3);
            this.firma.Name = "firma";
            this.firma.Size = new System.Drawing.Size(233, 20);
            this.firma.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Firma:";
            // 
            // uwagi
            // 
            this.uwagi.Edited = false;
            this.uwagi.Location = new System.Drawing.Point(12, 179);
            this.uwagi.Name = "uwagi";
            this.uwagi.Size = new System.Drawing.Size(622, 125);
            this.uwagi.TabIndex = 16;
            this.uwagi.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 163);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Informacje dodatkowe";
            // 
            // kod_pocztowy
            // 
            this.kod_pocztowy.Edited = false;
            this.kod_pocztowy.Location = new System.Drawing.Point(443, 133);
            this.kod_pocztowy.Mask = "00-000";
            this.kod_pocztowy.Name = "kod_pocztowy";
            this.kod_pocztowy.Size = new System.Drawing.Size(155, 20);
            this.kod_pocztowy.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(363, 136);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Kod pocztowy";
            // 
            // miejscowosc
            // 
            this.miejscowosc.Edited = false;
            this.miejscowosc.Location = new System.Drawing.Point(113, 133);
            this.miejscowosc.Name = "miejscowosc";
            this.miejscowosc.Size = new System.Drawing.Size(209, 20);
            this.miejscowosc.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 136);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Miejscowość:";
            // 
            // ulica
            // 
            this.ulica.Edited = false;
            this.ulica.Location = new System.Drawing.Point(113, 107);
            this.ulica.Name = "ulica";
            this.ulica.Size = new System.Drawing.Size(485, 20);
            this.ulica.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Ulica i nr domu:";
            // 
            // email
            // 
            this.email.Edited = false;
            this.email.Location = new System.Drawing.Point(418, 81);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(180, 20);
            this.email.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(363, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "E-mail:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pesel);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(27, 51);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.panel2.Size = new System.Drawing.Size(481, 27);
            this.panel2.TabIndex = 5;
            this.panel2.Visible = false;
            // 
            // pesel
            // 
            this.pesel.Edited = false;
            this.pesel.Location = new System.Drawing.Point(62, 3);
            this.pesel.Mask = "00000000000";
            this.pesel.Name = "pesel";
            this.pesel.Size = new System.Drawing.Size(180, 20);
            this.pesel.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "PESEL:";
            // 
            // nr_tel
            // 
            this.nr_tel.Edited = false;
            this.nr_tel.Location = new System.Drawing.Point(89, 81);
            this.nr_tel.Name = "nr_tel";
            this.nr_tel.Size = new System.Drawing.Size(233, 20);
            this.nr_tel.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Nr telefonu:";
            // 
            // imie
            // 
            this.imie.Edited = false;
            this.imie.Location = new System.Drawing.Point(418, 28);
            this.imie.Name = "imie";
            this.imie.Size = new System.Drawing.Size(180, 20);
            this.imie.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(363, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Imię:";
            // 
            // nazwisko
            // 
            this.nazwisko.Edited = false;
            this.nazwisko.Location = new System.Drawing.Point(89, 28);
            this.nazwisko.Name = "nazwisko";
            this.nazwisko.Size = new System.Drawing.Size(233, 20);
            this.nazwisko.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nazwisko:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 102F));
            this.tableLayoutPanel2.Controls.Add(this.button1, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.button2, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 415);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(622, 41);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Location = new System.Drawing.Point(523, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 35);
            this.button1.TabIndex = 5;
            this.button1.Text = "Zapisz zmiany";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Dock = System.Windows.Forms.DockStyle.Right;
            this.button2.Location = new System.Drawing.Point(433, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(84, 35);
            this.button2.TabIndex = 6;
            this.button2.Text = "Anuluj";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "005_Task_16x16_72.png");
            this.imageList1.Images.SetKeyName(1, "1446_envelope_stamp_clsd_32.png");
            this.imageList1.Images.SetKeyName(2, "settings_16.png");
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(1, 381);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(147, 32);
            this.button7.TabIndex = 6;
            this.button7.Text = "Dodaj polisę";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // databaseDataSet1
            // 
            this.databaseDataSet1.DataSetName = "databaseDataSet";
            this.databaseDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // polisyTableAdapter1
            // 
            this.polisyTableAdapter1.ClearBeforeFill = true;
            // 
            // prodKlientaTableAdapter1
            // 
            this.prodKlientaTableAdapter1.ClearBeforeFill = true;
            // 
            // produktyTableAdapter1
            // 
            this.produktyTableAdapter1.ClearBeforeFill = true;
            // 
            // EdytujKlienta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(652, 501);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EdytujKlienta";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Polisy";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EdytujKlienta_FormClosing);
            this.Load += new System.EventHandler(this.EdytujKlienta_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ratyGrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.clientMode.ResumeLayout(false);
            this.clientMode.PerformLayout();
            this.clientData.ResumeLayout(false);
            this.clientData.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox clientMode;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox clientData;
        private System.Windows.Forms.Panel panel1;
        private Controls.Masked regon;
        private System.Windows.Forms.Label label4;
        private Controls.TxtBox firma;
        private System.Windows.Forms.Label label3;
        private Controls.RichTxt uwagi;
        private System.Windows.Forms.Label label11;
        private Controls.Masked kod_pocztowy;
        private System.Windows.Forms.Label label10;
        private Controls.TxtBox miejscowosc;
        private System.Windows.Forms.Label label9;
        private Controls.TxtBox ulica;
        private System.Windows.Forms.Label label8;
        private Controls.Masked email;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private Controls.Masked pesel;
        private System.Windows.Forms.Label label6;
        private Controls.TxtBox nr_tel;
        private System.Windows.Forms.Label label5;
        private Controls.TxtBox imie;
        private System.Windows.Forms.Label label2;
        private Controls.TxtBox nazwisko;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label13;
        private Controls.TxtBox edytowany;
        private Controls.TxtBox dodany;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button anulujBtn;
        private System.Windows.Forms.Button zapiszBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView typPolisy;
        private System.Windows.Forms.ColumnHeader Rodzaj;
        private System.Windows.Forms.ColumnHeader Prowizja;
        private System.Windows.Forms.ColumnHeader Opis;
        private Controls.RichTxt richTxt1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RadioButton wznowionaRadio;
        private System.Windows.Forms.RadioButton nowaRadio;
        private System.Windows.Forms.Label label15;
        private Controls.TxtBox prowizjaTxt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView ratyGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn wys_raty;
        private System.Windows.Forms.DataGridViewTextBoxColumn term_zaplaty;
        private System.Windows.Forms.DataGridViewCheckBoxColumn zaplacono;
        private System.Windows.Forms.Label label17;
        private Controls.TxtBox skladka;
        private System.Windows.Forms.Label label18;
        private Controls.TxtBox raty;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private Controls.TxtBox suma;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DateTimePicker dataDo;
        private System.Windows.Forms.DateTimePicker dataOd;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DateTimePicker dataZawarcia;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button4;
        private Controls.TxtBox txtTyp;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TreeView treeView1;
        private Elements.databaseDataSet databaseDataSet1;
        private Elements.databaseDataSetTableAdapters.PolisyTableAdapter polisyTableAdapter1;
        private Elements.databaseDataSetTableAdapters.ProdKlientaTableAdapter prodKlientaTableAdapter1;
        private Elements.databaseDataSetTableAdapters.ProduktyTableAdapter produktyTableAdapter1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button button7;
    }
}