﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using StaticRegistry;

namespace Klienci
{
    public class Klient
    {
        public int ID;
        public String Nazwisko;
        public String Imie;
        public String Firma;
        public String PESEL;
        public String REGON;
        public String Nr_tel;
        public String E_mail;
        public String Ulica;
        public String Miejscowosc;
        public String Kod_pocztowy;
        public String Uwagi;

        public void DodajKlienta()
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("INSERT INTO [Klienci] (Nazwisko, Imie, Firma, PESEL, REGON, Nr_tel, E_mail, Ulica, Miejscowosc, Kod_pocztowy, Uwagi) VALUES (@Nazwisko, @Imie, @Firma, @PESEL, @REGON, @Nr_tel, @E_mail, @Ulica, @Miejscowosc, @Kod_pocztowy, @Uwagi)", conn);
                command.Parameters.AddWithValue("@Nazwisko", this.Nazwisko);
                command.Parameters.AddWithValue("@Imie", this.Imie);
                command.Parameters.AddWithValue("@Firma", this.Firma);
                command.Parameters.AddWithValue("@PESEL", this.PESEL);
                command.Parameters.AddWithValue("@REGON", this.REGON);
                command.Parameters.AddWithValue("@Nr_tel", this.Nr_tel);
                command.Parameters.AddWithValue("@E_mail", this.E_mail);
                command.Parameters.AddWithValue("@Ulica", this.Ulica);
                command.Parameters.AddWithValue("@Miejscowosc", this.Miejscowosc);
                command.Parameters.AddWithValue("@Kod_pocztowy", this.Kod_pocztowy);
                command.Parameters.AddWithValue("@Uwagi", this.Uwagi);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void ZapiszKlienta()
        {
            
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("UPDATE [Klienci] SET [Nazwisko]=@Nazwisko, [Imie]=@Imie, [Firma]=@Firma, [PESEL]=PESEL, [REGON]=@REGON, [Nr_tel]=@Nr_tel, [E_mail]=@E_mail, [Ulica]=@Ulica, [Miejscowosc]=@Miejscowosc, [Kod_pocztowy]=@Kod_pocztowy, [Uwagi]=@Uwagi WHERE Id_klienta=@ID;", conn);
                command.Parameters.AddWithValue("@ID", this.ID);
                command.Parameters.AddWithValue("@Nazwisko", this.Nazwisko);
                command.Parameters.AddWithValue("@Imie", this.Imie);
                command.Parameters.AddWithValue("@Firma", this.Firma);
                command.Parameters.AddWithValue("@PESEL", this.PESEL);
                command.Parameters.AddWithValue("@REGON", this.REGON);
                command.Parameters.AddWithValue("@Nr_tel", this.Nr_tel);
                command.Parameters.AddWithValue("@E_mail", this.E_mail);
                command.Parameters.AddWithValue("@Ulica", this.Ulica);
                command.Parameters.AddWithValue("@Miejscowosc", this.Miejscowosc);
                command.Parameters.AddWithValue("@Kod_pocztowy", this.Kod_pocztowy);
                command.Parameters.AddWithValue("@Uwagi", this.Uwagi);
                int aff = command.ExecuteNonQuery();
                
                conn.Close();
            }
        }
    }
}
