﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace StaticRegistry
{
    public static class Registry
    {
        
        private static SqlConnection sqlConn;

        public static string ConnString;

        public static SqlConnection SqlConn
        {
            set
            {
                sqlConn = value;
            }
            get
            {
                return sqlConn;
            }
        }

    }
}
