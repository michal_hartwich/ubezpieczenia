﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using StaticRegistry;

namespace Elements
{
    public class Grupa
    {
        public String Rodzaj;
        public String Opis;
        public String Prowizja;

        public void DodajGrupe()
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("INSERT INTO [Grupy] (Rodzaj, Opis, Prowizja) VALUES (@Rodzaj, @Opis, @Prowizja);", conn);
                command.Parameters.AddWithValue("@Rodzaj", this.Rodzaj);
                command.Parameters.AddWithValue("@Opis", this.Opis);
                command.Parameters.AddWithValue("@Prowizja", this.Prowizja);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void EdytujGrupe(int id)
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("UPDATE [Grupy] SET Rodzaj=@Rodzaj, Opis=@Opis, Prowizja=@Prowizja WHERE Id_grupy=@Id_grupy;", conn);
                command.Parameters.AddWithValue("@Id_grupy", id);
                command.Parameters.AddWithValue("@Rodzaj", this.Rodzaj);
                command.Parameters.AddWithValue("@Opis", this.Opis);
                command.Parameters.AddWithValue("@Prowizja", this.Prowizja);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public static void UsunGrupe(String id)
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("DELETE FROM Grupy WHERE Id_grupy='" + id + "';", conn);
                //command.Parameters.AddWithValue("@Id_produktu", int.Parse(id));

                command.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}
