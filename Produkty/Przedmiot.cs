﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using StaticRegistry;

namespace Elements
{
    public class Przedmiot
    {
        public String Id_przedmiotu;
        public String Id_grupy;
        public String Rodzaj;
        public String Opis;

        public void DodajPrzedmiot()
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("INSERT INTO [Przedmioty] (Id_grupy, Rodzaj, Opis) VALUES (@Id_grupy, @Rodzaj, @Opis);", conn);
                command.Parameters.AddWithValue("@Id_grupy", this.Id_grupy);
                command.Parameters.AddWithValue("@Rodzaj", this.Rodzaj);
                command.Parameters.AddWithValue("@Opis", this.Opis);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void EdytujPrzedmiot(String id)
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("UPDATE [Przedmioty] SET Id_grupy=@Id_grupy, Rodzaj=@Rodzaj, Opis=@Opis WHERE Id_przedmiotu=@Id_przedmiotu;", conn);
                command.Parameters.AddWithValue("@Id_przedmiotu", id);
                command.Parameters.AddWithValue("@Id_grupy", this.Id_grupy);
                command.Parameters.AddWithValue("@Rodzaj", this.Rodzaj);
                command.Parameters.AddWithValue("@Opis", this.Opis);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public static void UsunPrzedmiot(String id)
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("DELETE FROM Przedioty WHERE Id_przedmiotu='" + id + "';", conn);
                //command.Parameters.AddWithValue("@Id_produktu", int.Parse(id));

                command.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}
