﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Globalization;
using StaticRegistry;
using Controls;

namespace Elements
{
    public partial class PolisyForm : Form
    {
        private String polisaId;
        private String przedmiotId;
        private DataTable klienci;
        private DataTable typy;
        //private DataTable grupy;
        private String grupaId;
        private String klientId;
        private String rodzaj;
        private String klient;
        private DataTable filtered;
        private DataSet ds;
        private List<string> hint;
        private string rowStr;
        private int opMode;
        private bool edited;

        public PolisyForm()
        {
            InitializeComponent();
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();

                String strSQL = "SELECT Id_klienta, Nazwisko, Imie, Firma, PESEL, REGON, Ulica, Miejscowosc, Kod_pocztowy FROM Klienci;";
                SqlDataAdapter da = new SqlDataAdapter(strSQL, conn);
                klienci = new DataTable();
                da.Fill(klienci);//<-there is an exception

                String strSQL2 = "SELECT * FROM Produkty;";
                SqlDataAdapter da2 = new SqlDataAdapter(strSQL2, conn);
                typy = new DataTable();
                da2.Fill(typy);//<-there is an exception

                conn.Close();
            }

            foreach (DataRow row in klienci.Rows)
            {
                List<string> itemStr = new List<string>();
                int i = 1;
                while (i < klienci.Columns.Count)
                {
                    itemStr.Add(row[i] + "");
                    i++;
                }
                itemStr.Add(row[0] + "");
                ListViewItem item = new ListViewItem(itemStr.ToArray());
                this.hintList.Items.Add(item);
               
            }
            DataTable grupy = this.databaseDataSet1.Grupy;
            foreach (DataRow row in grupy.Rows)
            {
                List<string> itemStr = new List<string>();
                int i = 1;
                while (i < grupy.Columns.Count)
                {
                    itemStr.Add(row[i] + "");
                    i++;
                }
                itemStr.Add(row[0] + "");
                ListViewItem item = new ListViewItem(itemStr.ToArray());
                this.grupaPolisy.Items.Add(item);

            }

            this.dataOd.Value = this.dataZawarcia.Value.AddDays(1);
            this.dataDo.Value = this.dataOd.Value.AddYears(1).AddDays(-1);
            //this.button4.Enabled = false;
           
            klientId = "";
            grupaId = "";
            klient = "";
            rodzaj = "";
            polisaId = "";
            przedmiotId = "";
            this.opMode = 0;
            this.edited = false;

            //foreach (Control ctrl in groupBox1.Controls)
            //{
            //    if (ctrl is TxtBox || ctrl is Masked)
            //    {
            //        ctrl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.control_KeyDown);
            //    }
            //}
            foreach (Control ctrl in groupBox2.Controls)
            {
                if (ctrl is TxtBox || ctrl is Masked)
                {
                    ctrl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.control_KeyDown);
                }
            }
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.control_KeyDown);
        }

        public PolisyForm(String idPolisy, String idPrzedmiotu)
        {
            this.polisaId = idPolisy;
            this.przedmiotId = idPrzedmiotu;
            InitializeComponent();
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();

                String strSQL = "SELECT Id_klienta, Nazwisko, Imie, Firma, PESEL, REGON, Ulica, Miejscowosc, Kod_pocztowy FROM Klienci;";
                SqlDataAdapter da = new SqlDataAdapter(strSQL, conn);
                klienci = new DataTable();
                da.Fill(klienci);//<-there is an exception

                String strSQL2 = "SELECT * FROM Produkty;";
                SqlDataAdapter da2 = new SqlDataAdapter(strSQL2, conn);
                typy = new DataTable();
                da2.Fill(typy);//<-there is an exception

                conn.Close();
            }

            foreach (DataRow row in klienci.Rows)
            {
                List<string> itemStr = new List<string>();
                int i = 1;
                while (i < klienci.Columns.Count)
                {
                    itemStr.Add(row[i] + "");
                    i++;
                }
                itemStr.Add(row[0] + "");
                ListViewItem item = new ListViewItem(itemStr.ToArray());
                this.hintList.Items.Add(item);

            }

            DataTable grupy = this.databaseDataSet1.Grupy;
            foreach (DataRow row in grupy.Rows)
            {
                List<string> itemStr = new List<string>();
                int i = 1;
                while (i < grupy.Columns.Count)
                {
                    itemStr.Add(row[i] + "");
                    i++;
                }
                itemStr.Add(row[0] + "");
                ListViewItem item = new ListViewItem(itemStr.ToArray());
                this.grupaPolisy.Items.Add(item);

            }
            this.opMode = 1;
            this.txtSearch.Enabled = false;
            this.button1.Enabled = false;
            zapiszBtn.Text = "Zapisz";

            //foreach (Control ctrl in groupBox1.Controls)
            //{
            //    if (ctrl is TxtBox || ctrl is Masked)
            //    {
            //        ctrl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.control_KeyDown);
            //    }
            //}
            foreach (Control ctrl in groupBox2.Controls)
            {
                if (ctrl is TxtBox || ctrl is Masked)
                {
                    ctrl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.control_KeyDown);
                }
            }
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.control_KeyDown);
        }

        private void control_KeyDown(object sender, KeyEventArgs e)
        {
            this.edited = true;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void PolisyForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'databaseDataSet.Klienci' table. You can move, or remove it, as needed.
            this.klienciTableAdapter.Fill(this.databaseDataSet1.Klienci);
            this.polisyTableAdapter1.Fill(this.databaseDataSet1.Polisy);
            this.produktyTableAdapter1.Fill(this.databaseDataSet1.Produkty);
            this.prodKlientaTableAdapter1.Fill(this.databaseDataSet1.ProdKlienta);
            this.grupyTableAdapter1.Fill(this.databaseDataSet1.Grupy);
            this.przedmiotyTableAdapter1.Fill(this.databaseDataSet1.Przedmioty);
            this.przedmiotyGridViewTableAdapter1.Fill(this.databaseDataSet1.PrzedmiotyGridView);

            if (this.polisaId!="")
            {
                RefreshItems();
                if (this.polisaId != "")
                {
                    this.LoadDataToForm(this.przedmiotId);
                }
            }

            DataTable grupy = this.databaseDataSet1.Grupy;
            foreach (DataRow row in grupy.Rows)
            {
                List<string> itemStr = new List<string>();
                int i = 1;
                while (i < grupy.Columns.Count)
                {
                    itemStr.Add(row[i] + "");
                    i++;
                }
                itemStr.Add(row[0] + "");
                ListViewItem item = new ListViewItem(itemStr.ToArray());
                this.grupaPolisy.Items.Add(item);

            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            
                String txt = txtSearch.Text;
                DataRow[] rows = klienci.Select("REGON LIKE '" + txt + "%' OR Nazwisko LIKE '"+txt+"%' OR Imie LIKE '"+txt+"%' OR Firma LIKE '"+txt+"%' OR PESEL LIKE '"+txt+"%' OR Ulica LIKE '"+txt+"%' OR Miejscowosc LIKE '"+txt+"%' OR Kod_pocztowy LIKE '"+txt+"%'");
                hintList.Items.Clear();
                foreach (DataRow row in rows)
                {
                    List<string> itemStr = new List<string>();
                    int i = 1;
                    while (i < klienci.Columns.Count)
                    {
                        itemStr.Add(row[i] + "");
                        i++;
                    }
                    itemStr.Add(row[0] + "");
                    ListViewItem item = new ListViewItem(itemStr.ToArray());
                    this.hintList.Items.Add(item);
                }
        }

       

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Down)
            {
                this.hintList.Focus();
                this.hintList.Select();
            }
        }

        private void hintList_DoubleClick(object sender, EventArgs e)
        {
            String klientStr ="";
            foreach (ListViewItem.ListViewSubItem item in this.hintList.SelectedItems[0].SubItems)
            {
                klientStr += item.Text + " ";
            }
            klientId = this.hintList.SelectedItems[0].SubItems[this.hintList.SelectedItems[0].SubItems.Count-1].Text + "";
            this.klient = this.hintList.SelectedItems[0].SubItems[0].Text + " "+ this.hintList.SelectedItems[0].SubItems[1].Text + " " +this.hintList.SelectedItems[0].SubItems[2].Text;
            this.hintList.Visible = false;
            this.txtSearch.Text = klientStr;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.hintList.Visible)
            {
                this.hintList.Visible = false;
            }
            else
            {
                this.hintList.Visible = true;
            }
        }

        private void hintList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void typPolisy_DoubleClick(object sender, EventArgs e)
        {
            String typyStr = "";
            int i = 0;
            foreach (ListViewItem.ListViewSubItem item in this.grupaPolisy.SelectedItems[0].SubItems)
            {
                typyStr += item.Text + " ";
                i++;
                if (i == 3) break;
            }
            grupaId = this.grupaPolisy.SelectedItems[0].SubItems[this.grupaPolisy.SelectedItems[0].SubItems.Count - 1].Text + "";
            this.rodzaj = this.grupaPolisy.SelectedItems[0].SubItems[0].Text;
            //this.prowizjaTxt.Text = this.grupaPolisy.SelectedItems[0].SubItems[1].Text;
            this.grupaPolisy.Visible = false;
            this.txtTyp.Text = typyStr;

            this.przedmiotyList.Items.Clear();
            DataRow[] rows = this.databaseDataSet1.PrzedmiotyGridView.Select("Id_grupy='" + grupaId + "'");
            foreach (DataRow row in rows)
            {
                List<string> itemStr = new List<string>();
                int j = 2;
                while (j < this.databaseDataSet1.PrzedmiotyGridView.Columns.Count)
                {
                    itemStr.Add(row[j] + "");
                    j++;
                }
                itemStr.Add(row[0] + "");
                ListViewItem item = new ListViewItem(itemStr.ToArray());
                this.przedmiotyList.Items.Add(item);

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (DataRow row in typy.Rows)
            {
                List<string> itemStr = new List<string>();
                int i = 1;
                while (i < typy.Columns.Count)
                {
                    itemStr.Add(row[i] + "");
                    i++;
                }
                itemStr.Add(row[0] + "");
                ListViewItem item = new ListViewItem(itemStr.ToArray());
                this.grupaPolisy.Items.Add(item);

            }
            if (this.grupaPolisy.Visible)
            {
                this.grupaPolisy.Visible = false;
            }
            else
            {
                this.grupaPolisy.Visible = true;
            }
        }

        private void txtTyp_TextChanged(object sender, EventArgs e)
        {
            String txt = txtTyp.Text;
            DataRow[] rows = typy.Select("Rodzaj LIKE '" + txt + "%' OR Prowizja LIKE '" + txt + "%' OR Opis LIKE '" + txt + "%'");
            grupaPolisy.Items.Clear();
            foreach (DataRow row in rows)
            {
                List<string> itemStr = new List<string>();
                int i = 1;
                while (i < typy.Columns.Count)
                {
                    itemStr.Add(row[i] + "");
                    i++;
                }
                itemStr.Add(row[0] + "");
                ListViewItem item = new ListViewItem(itemStr.ToArray());
                this.grupaPolisy.Items.Add(item);
            }
        }

        private void typPolisy_MouseEnter(object sender, EventArgs e)
        {
            this.grupaPolisy.Focus();
            this.grupaPolisy.Select();
        }

        private void typPolisy_MouseLeave(object sender, EventArgs e)
        {
            this.txtTyp.Focus();
            this.txtTyp.Select();
        }

        private void dataZawarcia_ValueChanged(object sender, EventArgs e)
        {
            this.dataOd.Value = this.dataZawarcia.Value.AddDays(1);
            this.dataDo.Value = this.dataOd.Value.AddYears(1).AddDays(-1);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.ratyGrid.Rows.Clear();
            String skladkaStr = this.skladka.Text;
            Decimal skladka = 0;
            if (skladkaStr!="")
            {
                skladka = Decimal.Parse(skladkaStr, CultureInfo.CurrentCulture);
            }
            else
            {
                MessageBox.Show("Proszę podać składkę ubezpieczenia.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            int iloscRat = 0;
            if (this.raty.Text != "")
            {
                try
                {
                    iloscRat = int.Parse(this.raty.Text);
                }
                catch(Exception ee)
                {
                    MessageBox.Show("Proszę podać liczbę całkowitą w polu ilość rat.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Proszę podać ilość rat.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Decimal rata = skladka / iloscRat;
            Decimal.Round(rata, 2, MidpointRounding.ToEven);
            int daty = 365 / iloscRat;
            DateTime data = DateTime.Now;
            for (int i = 0; i < iloscRat; i++)
            {
                this.ratyGrid.Rows.Add(rata.ToString("N2"), data.AddDays(i*daty).ToString("dd-MM-yyyy"), false);
            }
        }

        private void zapiszBtn_Click(object sender, EventArgs e)
        {
            if (this.klientId=="")
            {
                MessageBox.Show("Proszę wybrać klienta.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (this.grupaId == "")
            {
                MessageBox.Show("Proszę wybrać typ ubezpieczenia.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DialogResult r;
            if (this.opMode==0)
            {
                r = MessageBox.Show("Czy chcesz dodać ten przedmiot do polisy?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            else
            {
                r= MessageBox.Show("Czy chcesz zapisać zmiany?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            if (r == DialogResult.Yes)
            {
               
            }
            else
            {
                return;
            }
            String raty = "";
            int j = 0;
            foreach(DataGridViewRow row in this.ratyGrid.Rows)
            {
                int i = 0;
                foreach(DataGridViewCell cell in row.Cells)
                {
                    raty += cell.Value + "";
                    i++;
                    if(i != row.Cells.Count)
                    {
                        raty += "|";
                    }
                }
                j++;
                if (j != ratyGrid.Rows.Count)
                {
                    raty += "#";
                }
            }
            ProdKlienta prod = new ProdKlienta();
            if (this.polisaId == "")
            {
                Polisa polisa = new Polisa();
                polisa.Id_klienta = this.klientId;
                polisa.Nazwa = this.rodzaj + " " + this.klient;
                polisa.DodajPolise();

                DataTable polisaOst;
                using (SqlConnection conn = new SqlConnection(Registry.ConnString))
                {
                    conn.Open();

                    String strSQL2 = "SELECT * FROM Polisy;";
                    SqlDataAdapter da2 = new SqlDataAdapter(strSQL2, conn);
                    polisaOst = new DataTable();
                    da2.Fill(polisaOst);//<-there is an exception

                    conn.Close();
                }
                polisaOst.DefaultView.Sort = "Id_polisy DESC";
                polisaOst = polisaOst.DefaultView.ToTable();
                prod.Id_polisy = polisaOst.Rows[0][0].ToString();
                this.polisaId = polisaOst.Rows[0][0].ToString();
            }
            else
            {
                prod.Id_polisy = this.polisaId;
            }
            prod.Id_typu = this.grupaId;
            prod.Okres_od = this.dataOd.Text;
            prod.Okres_do = this.dataDo.Text;
            //prod.Suma_ubezpieczenia = this.suma.Text;
            prod.Skladka = this.skladka.Text;
            prod.Ilosc_rat = this.raty.Text;
            prod.Raty = raty;
            prod.Data_zawarcia = this.dataZawarcia.Text;
            prod.Uwagi = this.uwagi.Text;
            if (this.nowaRadio.Checked)
            {
                prod.Status = "1";
            }
            else
            {
                prod.Status = "0";
            }
            if (this.opMode == 0)
            {
                prod.DodajProdKlienta();
                this.prodKlientaTableAdapter1.Fill(this.databaseDataSet1.ProdKlienta);
                this.polisyTableAdapter1.Fill(this.databaseDataSet1.Polisy);
                RefreshItems();
                this.txtSearch.Enabled = false;
            }
            else
            {
                prod.EdytujProdKlienta(this.przedmiotId);
                PolisyForm_Load(null, null);
            }
            this.edited = false;
            

            
        }

        private void RefreshItems()
        {
            DataTable przedmioty = new DataTable();
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();

                String strSQL = "SELECT * FROM ProdKlienta WHERE Id_polisy='" + this.polisaId + "';";
                SqlDataAdapter da = new SqlDataAdapter(strSQL, conn);
                da.Fill(przedmioty);//<-there is an exceptio
                conn.Close();
            }

            //this.przedmiotyList.Items.Clear();

            foreach (DataRow row in przedmioty.Rows)
            {
                List<String> strstr = new List<String>();
                DataRow[] nazwaTypu = this.typy.Select("Id_produktu='" + row["Id_typu"] + "'");
                strstr.Add(nazwaTypu[0]["Rodzaj"].ToString());
                strstr.Add(row["Id_prodKlienta"].ToString());
                ListViewItem item = new ListViewItem(strstr.ToArray());
                //this.przedmiotyList.Items.Add(item);
            }

            if (przedmioty.Rows.Count == 0)
            {
                //this.button4.Enabled = false;
                //this.button5.Enabled = false;
            }
            else
            {
                //this.button4.Enabled = true;
                //this.button5.Enabled = true;
            }
        }

        private void LoadDataToForm(String idPrzedmiotu)
        {
            DataTable klienci = this.databaseDataSet1.Klienci;
            DataTable polisy = this.databaseDataSet1.Polisy;
            DataTable typy = this.databaseDataSet1.Produkty;
            DataTable przedmioty = this.databaseDataSet1.ProdKlienta;

            DataRow[] przedmiot = przedmioty.Select("Id_prodKlienta='"+idPrzedmiotu+"'");
            DataRow[] polisa = polisy.Select("Id_polisy='" + this.polisaId + "'");
            DataRow[] klient = klienci.Select("Id_klienta='"+polisa[0]["Id_klienta"].ToString()+"'");
            String search = "";
            for (int i = 1; i < klienci.Columns.Count-1; i++ )
            {
                search += klient[0][i].ToString() + " ";
            }

            this.txtSearch.Text = search;

            DataRow[] typ = typy.Select("Id_produktu='" + przedmiot[0]["Id_typu"].ToString() + "'");
            String txt = "";
            for (int i = 1; i < typy.Columns.Count - 1; i++)
            {
                txt += typ[0][i].ToString() + " ";
            }

            this.grupaId = przedmiot[0]["Id_typu"].ToString();

            this.txtTyp.Text = txt;
            //this.prowizjaTxt.Text = typ[0]["Prowizja"].ToString();
            this.dataZawarcia.Value = DateTime.ParseExact(przedmiot[0]["Data_zawarcia"].ToString(), "dd-MM-yyyy",null); //Convert.ToDateTime(przedmiot[0]["Data_zawarcia"].ToString()); //DateTime.ParseExact(przedmiot[0]["Data_zawarcia"].ToString(), "dd-MM-yyy",null);
            this.dataOd.Value = DateTime.ParseExact(przedmiot[0]["Okres_od"].ToString(), "dd-MM-yyyy", null);
            this.dataDo.Value = DateTime.ParseExact(przedmiot[0]["Okres_do"].ToString(), "dd-MM-yyyy", null);
            this.skladka.Text = przedmiot[0]["Skladka"].ToString();
            //this.suma.Text = przedmiot[0]["Suma_ubezpieczenia"].ToString();
            this.raty.Text = przedmiot[0]["Ilosc_rat"].ToString();
            this.uwagi.Text = przedmiot[0]["Uwagi"].ToString();
            String[] rows = przedmiot[0]["Raty"].ToString().Split('#');
            this.ratyGrid.Rows.Clear();
            foreach(String row in rows)
            {
                String[] cells = row.Split('|');
                this.ratyGrid.Rows.Add(cells);
            }
            if (przedmiot[0]["Status"].ToString().Equals("1"))
            {
                this.nowaRadio.Checked = true;
                this.wznowionaRadio.Checked = false;
            }
            else
            {
                this.nowaRadio.Checked = false;
                this.wznowionaRadio.Checked = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (this.edited)
            {
                DialogResult r = MessageBox.Show("Zawartość niektórych pól została zmieniona. Wprowadzone zmiany zostaną utracone.\nKontynuować?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    this.edited = false;
                }
                else
                {
                    return;
                }
            }
            //this.txtSearch.Text = "";
            //foreach (Control ctrl in groupBox1.Controls)
            //{
            //    if (ctrl is TxtBox || ctrl is Masked)
            //    {
            //        ctrl.Text = "";
            //    }
            //}
            foreach (Control ctrl in groupBox2.Controls)
            {
                if (ctrl is TxtBox || ctrl is Masked)
                {
                    ctrl.Text = "";
                }
            }
            ratyGrid.Rows.Clear();
            this.opMode = 0;
            this.zapiszBtn.Text = "Dodaj";
        }

        private void przedmiotyList_Click(object sender, EventArgs e)
        {
            if (this.edited)
            {
                DialogResult r = MessageBox.Show("Zawartość niektórych pól została zmieniona. Wprowadzone zmiany zostaną utracone.\nKontynuować?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    
                }
                else
                {
                    return;
                }
            }
            //this.LoadDataToForm(this.przedmiotyList.SelectedItems[0].SubItems[1].Text);
            //this.przedmiotId = this.przedmiotyList.SelectedItems[0].SubItems[1].Text;
            this.opMode = 1;
            this.zapiszBtn.Text = "Zapisz";
            this.edited = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //DialogResult r = MessageBox.Show("Czy chcesz usunąć \"" + this.przedmiotyList.SelectedItems[0].SubItems[0].Text + "\" z polisy?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //if (r == System.Windows.Forms.DialogResult.Yes)
            //{
            //    //ProdKlienta.UsunProdKlienta(this.przedmiotyList.SelectedItems[0].SubItems[1].Text);
            //    //RefreshItems();

            //    //foreach (Control ctrl in groupBox1.Controls)
            //    //{
            //    //    if (ctrl is TxtBox || ctrl is Masked)
            //    //    {
            //    //        ctrl.Text = "";
            //    //    }
            //    //}
            //    foreach (Control ctrl in groupBox2.Controls)
            //    {
            //        if (ctrl is TxtBox || ctrl is Masked)
            //        {
            //            ctrl.Text = "";
            //        }
            //    }
            //    ratyGrid.Rows.Clear();
            //    this.opMode = 0;
            //    this.zapiszBtn.Text = "Dodaj";
            //}
        }

        private void ratyGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.edited = true;
        }

        private void PolisyForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.edited)
            {
                DialogResult r = MessageBox.Show("Zawartość niektórych elementów została zmieniona. Wprowadzone zmiany zostaną utracone.\nKontynuować?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    e.Cancel = false;
                    this.Hide();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void anulujBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (this.grupaPolisy.Visible)
            {
                this.grupaPolisy.Visible = false;
            }
            else
            {
                this.grupaPolisy.Visible = true;
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            if (this.przedmiotyList.Visible)
            {
                this.przedmiotyList.Visible = false;
            }
            else
            {
                this.przedmiotyList.Visible = true;
            }
        }

        private void przedmiotyList_DoubleClick(object sender, EventArgs e)
        {
            //this.grupaPolisy.SelectedItems[0].SubItems[0].Text;
            this.przedmiotyGrid.Rows.Add(this.przedmiotyList.SelectedItems[0].SubItems[0].Text, this.przedmiotyList.SelectedItems[0].SubItems[1].Text, this.grupaPolisy.SelectedItems[0].SubItems[1].Text);
            this.przedmiotyList.Visible = false;
        }

        private void przedmiotyGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                this.przedmiotyGrid.Rows.RemoveAt(this.przedmiotyGrid.CurrentCell.RowIndex);
            }
        }

        private void skladka_TextChanged(object sender, EventArgs e)
        {
            this.ratyGrid.Rows.Clear();
            String skladkaStr = this.skladka.Text;
            Decimal skladka = 0;
            if (skladkaStr != "")
            {
                skladka = Decimal.Parse(skladkaStr, CultureInfo.CurrentCulture);
            }
            int iloscRat = 0;
            if (this.raty.Text != "")
            {
                try
                {
                    iloscRat = int.Parse(this.raty.Text);
                }
                catch (Exception ee)
                {
                    MessageBox.Show("Proszę podać liczbę całkowitą w polu ilość rat.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            Decimal rata = skladka / iloscRat;
            Decimal.Round(rata, 2, MidpointRounding.ToEven);
            int daty = 365 / iloscRat;
            DateTime data = DateTime.Now;
            for (int i = 0; i < iloscRat; i++)
            {
                this.ratyGrid.Rows.Add(rata.ToString("N2"), data.AddDays(i * daty).ToString("dd-MM-yyyy"), false);
            }

        }

        private void PolisyForm_Click(object sender, EventArgs e)
        {
            this.przedmiotyList.Visible = false;
        }
    }
}
