﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using StaticRegistry;

namespace Elements
{
    public class ProdKlienta
    {
        public String Id_polisy;
        public String Id_typu;
        public String Okres_od;
        public String Okres_do;
        public String Suma_ubezpieczenia;
        public String Skladka;
        public String Ilosc_rat;
        public String Raty;
        public String Data_zawarcia;
        public String Status;
        public String Uwagi;

        public void DodajProdKlienta()
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("INSERT INTO [ProdKlienta] (Id_polisy, Id_typu, Okres_od, Okres_do, Suma_ubezpieczenia, Skladka, Ilosc_rat, Raty, Data_zawarcia, Uwagi, Status) VALUES (@Id_polisy, @Id_typu, @Okres_od, @Okres_do, @Suma_ubezpieczenia, @Skladka, @Ilosc_rat, @Raty, @Data_zawarcia, @Uwagi, @Status);", conn);
                command.Parameters.AddWithValue("@Id_polisy", this.Id_polisy);
                command.Parameters.AddWithValue("@Id_typu", this.Id_typu);
                command.Parameters.AddWithValue("@Okres_od", this.Okres_od);
                command.Parameters.AddWithValue("@Okres_do", this.Okres_do);
                command.Parameters.AddWithValue("@Suma_ubezpieczenia", this.Suma_ubezpieczenia);
                command.Parameters.AddWithValue("@Skladka", this.Skladka);
                command.Parameters.AddWithValue("@Ilosc_rat", this.Ilosc_rat);
                command.Parameters.AddWithValue("@Raty", this.Raty);
                command.Parameters.AddWithValue("@Data_zawarcia", this.Data_zawarcia);
                command.Parameters.AddWithValue("@Uwagi", this.Uwagi);
                command.Parameters.AddWithValue("@Status", this.Status);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void EdytujProdKlienta(String id)
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("UPDATE [ProdKlienta] SET Id_polisy=@Id_polisy, Id_typu=@Id_typu, Okres_od=@Okres_od, Okres_do=@Okres_do, Suma_ubezpieczenia=@Suma_ubezpieczenia, Skladka=@Skladka, Ilosc_rat=@Ilosc_rat, Raty=@Raty, Data_zawarcia=@Data_zawarcia, Uwagi=@Uwagi, Status=@Status WHERE Id_prodKlienta='"+id+"';", conn);
                //command.Parameters.AddWithValue("@Id_produktu", int.Parse(id));
                command.Parameters.AddWithValue("@Id_polisy", this.Id_polisy);
                command.Parameters.AddWithValue("@Id_typu", this.Id_typu);
                command.Parameters.AddWithValue("@Okres_od", this.Okres_od);
                command.Parameters.AddWithValue("@Okres_do", this.Okres_do);
                command.Parameters.AddWithValue("@Suma_ubezpieczenia", this.Suma_ubezpieczenia);
                command.Parameters.AddWithValue("@Skladka", this.Skladka);
                command.Parameters.AddWithValue("@Ilosc_rat", this.Ilosc_rat);
                command.Parameters.AddWithValue("@Raty", this.Raty);
                command.Parameters.AddWithValue("@Data_zawarcia", this.Data_zawarcia);
                command.Parameters.AddWithValue("@Uwagi", this.Uwagi);
                command.Parameters.AddWithValue("@Status", this.Status);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public static void UsunProdKlienta(String id)
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("DELETE FROM ProdKlienta WHERE Id_prodKlienta='" + id + "';", conn);
                //command.Parameters.AddWithValue("@Id_produktu", int.Parse(id));
               
                command.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}
