﻿namespace Elements
{
    partial class PolisyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button1 = new System.Windows.Forms.Button();
            this.txtSearch = new Controls.TxtBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.anulujBtn = new System.Windows.Forms.Button();
            this.zapiszBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.przedmiotyList = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.przedmiotyGrid = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.txtTyp = new Controls.TxtBox();
            this.uwagi = new Controls.RichTxt();
            this.label2 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.wznowionaRadio = new System.Windows.Forms.RadioButton();
            this.nowaRadio = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ratyGrid = new System.Windows.Forms.DataGridView();
            this.wys_raty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.term_zaplaty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zaplacono = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label11 = new System.Windows.Forms.Label();
            this.skladka = new Controls.TxtBox();
            this.label10 = new System.Windows.Forms.Label();
            this.raty = new Controls.TxtBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dataDo = new System.Windows.Forms.DateTimePicker();
            this.dataOd = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dataZawarcia = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.hintList = new System.Windows.Forms.ListView();
            this.Nazwisko = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Imie = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Firma = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PESEL = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.REGON = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Ulica = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Miejscowosc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Kod_pocztowy = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bindingSource1 = new System.Windows.Forms.BindingSource();
            this.klienciBindingSource = new System.Windows.Forms.BindingSource();
            this.grupaPolisy = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Prowizja = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Opis = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button4 = new System.Windows.Forms.Button();
            this.klienciTableAdapter = new Elements.databaseDataSetTableAdapters.KlienciTableAdapter();
            this.databaseDataSet1 = new Elements.databaseDataSet();
            this.klienciTableAdapter1 = new Elements.databaseDataSetTableAdapters.KlienciTableAdapter();
            this.polisyTableAdapter1 = new Elements.databaseDataSetTableAdapters.PolisyTableAdapter();
            this.prodKlientaTableAdapter1 = new Elements.databaseDataSetTableAdapters.ProdKlientaTableAdapter();
            this.produktyTableAdapter1 = new Elements.databaseDataSetTableAdapters.ProduktyTableAdapter();
            this.prodKlientaTableAdapter2 = new Elements.databaseDataSetTableAdapters.ProdKlientaTableAdapter();
            this.grupyTableAdapter1 = new Elements.databaseDataSetTableAdapters.GrupyTableAdapter();
            this.przedmiotyTableAdapter1 = new Elements.databaseDataSetTableAdapters.PrzedmiotyTableAdapter();
            this.przedmiotyGridViewTableAdapter1 = new Elements.databaseDataSetTableAdapters.PrzedmiotyGridViewTableAdapter();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBox1 = new Controls.TxtBox();
            this.RodzajCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GrupaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProwizjaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SumaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.przedmiotyGrid)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ratyGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.klienciBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            this.splitContainer1.Panel1.Controls.Add(this.txtSearch);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Size = new System.Drawing.Size(558, 581);
            this.splitContainer1.SplitterDistance = 56;
            this.splitContainer1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(519, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 20);
            this.button1.TabIndex = 2;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.LemonChiffon;
            this.txtSearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSearch.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtSearch.Edited = false;
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtSearch.Location = new System.Drawing.Point(119, 12);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(394, 20);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Wybierz klienta:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(558, 521);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.anulujBtn);
            this.panel1.Controls.Add(this.zapiszBtn);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(554, 517);
            this.panel1.TabIndex = 2;
            // 
            // anulujBtn
            // 
            this.anulujBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.anulujBtn.Location = new System.Drawing.Point(358, 481);
            this.anulujBtn.Margin = new System.Windows.Forms.Padding(2);
            this.anulujBtn.Name = "anulujBtn";
            this.anulujBtn.Size = new System.Drawing.Size(91, 32);
            this.anulujBtn.TabIndex = 3;
            this.anulujBtn.Text = "Zamknij";
            this.anulujBtn.UseVisualStyleBackColor = true;
            this.anulujBtn.Click += new System.EventHandler(this.anulujBtn_Click);
            // 
            // zapiszBtn
            // 
            this.zapiszBtn.Location = new System.Drawing.Point(453, 481);
            this.zapiszBtn.Margin = new System.Windows.Forms.Padding(2);
            this.zapiszBtn.Name = "zapiszBtn";
            this.zapiszBtn.Size = new System.Drawing.Size(91, 32);
            this.zapiszBtn.TabIndex = 14;
            this.zapiszBtn.Text = "Zapisz";
            this.zapiszBtn.UseVisualStyleBackColor = true;
            this.zapiszBtn.Click += new System.EventHandler(this.zapiszBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtBox1);
            this.groupBox2.Controls.Add(this.przedmiotyList);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.txtTyp);
            this.groupBox2.Controls.Add(this.uwagi);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.wznowionaRadio);
            this.groupBox2.Controls.Add(this.nowaRadio);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.skladka);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.raty);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.dataDo);
            this.groupBox2.Controls.Add(this.dataOd);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.dataZawarcia);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(2, 2);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(552, 479);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Polisa Majątkowa";
            // 
            // przedmiotyList
            // 
            this.przedmiotyList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.przedmiotyList.FullRowSelect = true;
            this.przedmiotyList.GridLines = true;
            this.przedmiotyList.Location = new System.Drawing.Point(10, 70);
            this.przedmiotyList.Name = "przedmiotyList";
            this.przedmiotyList.Size = new System.Drawing.Size(532, 114);
            this.przedmiotyList.TabIndex = 34;
            this.przedmiotyList.UseCompatibleStateImageBehavior = false;
            this.przedmiotyList.View = System.Windows.Forms.View.Details;
            this.przedmiotyList.Visible = false;
            this.przedmiotyList.DoubleClick += new System.EventHandler(this.przedmiotyList_DoubleClick);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Rodzaj";
            this.columnHeader2.Width = 117;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Grupa";
            this.columnHeader3.Width = 114;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Opis";
            this.columnHeader4.Width = 254;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.przedmiotyGrid);
            this.groupBox1.Location = new System.Drawing.Point(7, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(535, 131);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Przedmioty";
            // 
            // przedmiotyGrid
            // 
            this.przedmiotyGrid.AllowUserToAddRows = false;
            this.przedmiotyGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.przedmiotyGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.przedmiotyGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.przedmiotyGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RodzajCol,
            this.GrupaCol,
            this.ProwizjaCol,
            this.SumaCol});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.przedmiotyGrid.DefaultCellStyle = dataGridViewCellStyle8;
            this.przedmiotyGrid.Location = new System.Drawing.Point(3, 22);
            this.przedmiotyGrid.Name = "przedmiotyGrid";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.przedmiotyGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.przedmiotyGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.przedmiotyGrid.Size = new System.Drawing.Size(532, 101);
            this.przedmiotyGrid.TabIndex = 0;
            this.przedmiotyGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.przedmiotyGrid_KeyDown);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(519, 18);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(27, 21);
            this.button2.TabIndex = 17;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // txtTyp
            // 
            this.txtTyp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTyp.Edited = false;
            this.txtTyp.Location = new System.Drawing.Point(194, 19);
            this.txtTyp.Margin = new System.Windows.Forms.Padding(2);
            this.txtTyp.Name = "txtTyp";
            this.txtTyp.Size = new System.Drawing.Size(321, 20);
            this.txtTyp.TabIndex = 16;
            // 
            // uwagi
            // 
            this.uwagi.Edited = false;
            this.uwagi.Location = new System.Drawing.Point(9, 423);
            this.uwagi.Margin = new System.Windows.Forms.Padding(2);
            this.uwagi.Name = "uwagi";
            this.uwagi.Size = new System.Drawing.Size(533, 52);
            this.uwagi.TabIndex = 13;
            this.uwagi.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(11, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Wybierz grupę ubezpieczenia:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 408);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Uwagi:";
            // 
            // wznowionaRadio
            // 
            this.wznowionaRadio.AutoSize = true;
            this.wznowionaRadio.Checked = true;
            this.wznowionaRadio.Location = new System.Drawing.Point(315, 379);
            this.wznowionaRadio.Margin = new System.Windows.Forms.Padding(2);
            this.wznowionaRadio.Name = "wznowionaRadio";
            this.wznowionaRadio.Size = new System.Drawing.Size(81, 17);
            this.wznowionaRadio.TabIndex = 12;
            this.wznowionaRadio.TabStop = true;
            this.wznowionaRadio.Text = "Wznowiona";
            this.wznowionaRadio.UseVisualStyleBackColor = true;
            // 
            // nowaRadio
            // 
            this.nowaRadio.AutoSize = true;
            this.nowaRadio.Location = new System.Drawing.Point(213, 379);
            this.nowaRadio.Margin = new System.Windows.Forms.Padding(2);
            this.nowaRadio.Name = "nowaRadio";
            this.nowaRadio.Size = new System.Drawing.Size(53, 17);
            this.nowaRadio.TabIndex = 11;
            this.nowaRadio.Text = "Nowa";
            this.nowaRadio.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 381);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Status:";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button3.Location = new System.Drawing.Point(447, 232);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(94, 27);
            this.button3.TabIndex = 9;
            this.button3.Text = "Oblicz raty";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ratyGrid);
            this.groupBox3.Location = new System.Drawing.Point(6, 263);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(537, 112);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Raty";
            // 
            // ratyGrid
            // 
            this.ratyGrid.AllowUserToAddRows = false;
            this.ratyGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ratyGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.ratyGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ratyGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.wys_raty,
            this.term_zaplaty,
            this.zaplacono});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ratyGrid.DefaultCellStyle = dataGridViewCellStyle11;
            this.ratyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ratyGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.ratyGrid.Location = new System.Drawing.Point(2, 15);
            this.ratyGrid.Margin = new System.Windows.Forms.Padding(2);
            this.ratyGrid.Name = "ratyGrid";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ratyGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.ratyGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ratyGrid.Size = new System.Drawing.Size(533, 95);
            this.ratyGrid.TabIndex = 10;
            this.ratyGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ratyGrid_CellClick);
            // 
            // wys_raty
            // 
            this.wys_raty.HeaderText = "Wysokość raty";
            this.wys_raty.Name = "wys_raty";
            this.wys_raty.Width = 150;
            // 
            // term_zaplaty
            // 
            this.term_zaplaty.HeaderText = "Termin zapłaty";
            this.term_zaplaty.Name = "term_zaplaty";
            this.term_zaplaty.Width = 150;
            // 
            // zaplacono
            // 
            this.zaplacono.FalseValue = "0";
            this.zaplacono.HeaderText = "Zapłacono";
            this.zaplacono.Name = "zaplacono";
            this.zaplacono.TrueValue = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(288, 238);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "zł";
            // 
            // skladka
            // 
            this.skladka.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.skladka.Edited = false;
            this.skladka.Location = new System.Drawing.Point(141, 236);
            this.skladka.Margin = new System.Windows.Forms.Padding(2);
            this.skladka.Name = "skladka";
            this.skladka.Size = new System.Drawing.Size(143, 20);
            this.skladka.TabIndex = 7;
            this.skladka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.skladka.TextChanged += new System.EventHandler(this.skladka_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 238);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Składka:";
            // 
            // raty
            // 
            this.raty.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.raty.Edited = false;
            this.raty.Location = new System.Drawing.Point(398, 236);
            this.raty.Margin = new System.Windows.Forms.Padding(2);
            this.raty.Name = "raty";
            this.raty.Size = new System.Drawing.Size(47, 20);
            this.raty.TabIndex = 8;
            this.raty.Text = "1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(347, 238);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Ilość rat:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(372, 213);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "do:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(118, 213);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "od:";
            // 
            // dataDo
            // 
            this.dataDo.CustomFormat = "dd-MM-yyyy";
            this.dataDo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataDo.Location = new System.Drawing.Point(398, 212);
            this.dataDo.Margin = new System.Windows.Forms.Padding(2);
            this.dataDo.Name = "dataDo";
            this.dataDo.Size = new System.Drawing.Size(143, 20);
            this.dataDo.TabIndex = 5;
            // 
            // dataOd
            // 
            this.dataOd.CustomFormat = "dd-MM-yyyy";
            this.dataOd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataOd.Location = new System.Drawing.Point(141, 212);
            this.dataOd.Margin = new System.Windows.Forms.Padding(2);
            this.dataOd.Name = "dataOd";
            this.dataOd.Size = new System.Drawing.Size(143, 20);
            this.dataOd.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 213);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Okres ubezpieczenia:";
            // 
            // dataZawarcia
            // 
            this.dataZawarcia.CustomFormat = "dd-MM-yyyy";
            this.dataZawarcia.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZawarcia.Location = new System.Drawing.Point(398, 188);
            this.dataZawarcia.Margin = new System.Windows.Forms.Padding(2);
            this.dataZawarcia.Name = "dataZawarcia";
            this.dataZawarcia.Size = new System.Drawing.Size(143, 20);
            this.dataZawarcia.TabIndex = 3;
            this.dataZawarcia.ValueChanged += new System.EventHandler(this.dataZawarcia_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(316, 191);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Data zawarcia:";
            // 
            // hintList
            // 
            this.hintList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Nazwisko,
            this.Imie,
            this.Firma,
            this.PESEL,
            this.REGON,
            this.Ulica,
            this.Miejscowosc,
            this.Kod_pocztowy});
            this.hintList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.hintList.FullRowSelect = true;
            this.hintList.GridLines = true;
            this.hintList.Location = new System.Drawing.Point(119, 3800);
            this.hintList.Name = "hintList";
            this.hintList.Size = new System.Drawing.Size(473, 204);
            this.hintList.TabIndex = 2;
            this.hintList.UseCompatibleStateImageBehavior = false;
            this.hintList.View = System.Windows.Forms.View.Details;
            this.hintList.Visible = false;
            this.hintList.SelectedIndexChanged += new System.EventHandler(this.hintList_SelectedIndexChanged);
            this.hintList.DoubleClick += new System.EventHandler(this.hintList_DoubleClick);
            // 
            // Nazwisko
            // 
            this.Nazwisko.Text = "Nazwisko";
            this.Nazwisko.Width = 92;
            // 
            // Imie
            // 
            this.Imie.Text = "Imię";
            this.Imie.Width = 100;
            // 
            // Firma
            // 
            this.Firma.Text = "Firma";
            this.Firma.Width = 173;
            // 
            // PESEL
            // 
            this.PESEL.Text = "PESEL";
            this.PESEL.Width = 101;
            // 
            // REGON
            // 
            this.REGON.Text = "REGON";
            this.REGON.Width = 87;
            // 
            // Ulica
            // 
            this.Ulica.Text = "Ulica";
            this.Ulica.Width = 99;
            // 
            // Miejscowosc
            // 
            this.Miejscowosc.Text = "Miejscowość";
            // 
            // Kod_pocztowy
            // 
            this.Kod_pocztowy.Text = "Kod pocztowy";
            // 
            // grupaPolisy
            // 
            this.grupaPolisy.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.Prowizja,
            this.Opis});
            this.grupaPolisy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grupaPolisy.FullRowSelect = true;
            this.grupaPolisy.GridLines = true;
            this.grupaPolisy.Location = new System.Drawing.Point(197, 105);
            this.grupaPolisy.Margin = new System.Windows.Forms.Padding(2);
            this.grupaPolisy.MultiSelect = false;
            this.grupaPolisy.Name = "grupaPolisy";
            this.grupaPolisy.Size = new System.Drawing.Size(322, 140);
            this.grupaPolisy.TabIndex = 1;
            this.grupaPolisy.UseCompatibleStateImageBehavior = false;
            this.grupaPolisy.View = System.Windows.Forms.View.Details;
            this.grupaPolisy.Visible = false;
            this.grupaPolisy.DoubleClick += new System.EventHandler(this.typPolisy_DoubleClick);
            this.grupaPolisy.MouseEnter += new System.EventHandler(this.typPolisy_MouseEnter);
            this.grupaPolisy.MouseLeave += new System.EventHandler(this.typPolisy_MouseLeave);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Rodzaj";
            // 
            // Prowizja
            // 
            this.Prowizja.Text = "Prowizja";
            this.Prowizja.Width = 71;
            // 
            // Opis
            // 
            this.Opis.Text = "Opis";
            this.Opis.Width = 187;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(80, 108);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(112, 22);
            this.button4.TabIndex = 35;
            this.button4.Text = "Dodaj...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // klienciTableAdapter
            // 
            this.klienciTableAdapter.ClearBeforeFill = true;
            // 
            // databaseDataSet1
            // 
            this.databaseDataSet1.DataSetName = "databaseDataSet";
            this.databaseDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // klienciTableAdapter1
            // 
            this.klienciTableAdapter1.ClearBeforeFill = true;
            // 
            // polisyTableAdapter1
            // 
            this.polisyTableAdapter1.ClearBeforeFill = true;
            // 
            // prodKlientaTableAdapter1
            // 
            this.prodKlientaTableAdapter1.ClearBeforeFill = true;
            // 
            // produktyTableAdapter1
            // 
            this.produktyTableAdapter1.ClearBeforeFill = true;
            // 
            // prodKlientaTableAdapter2
            // 
            this.prodKlientaTableAdapter2.ClearBeforeFill = true;
            // 
            // grupyTableAdapter1
            // 
            this.grupyTableAdapter1.ClearBeforeFill = true;
            // 
            // przedmiotyTableAdapter1
            // 
            this.przedmiotyTableAdapter1.ClearBeforeFill = true;
            // 
            // przedmiotyGridViewTableAdapter1
            // 
            this.przedmiotyGridViewTableAdapter1.ClearBeforeFill = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 191);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Numer polisy";
            // 
            // txtBox1
            // 
            this.txtBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBox1.Edited = false;
            this.txtBox1.Location = new System.Drawing.Point(141, 188);
            this.txtBox1.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox1.Name = "txtBox1";
            this.txtBox1.Size = new System.Drawing.Size(143, 20);
            this.txtBox1.TabIndex = 35;
            this.txtBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RodzajCol
            // 
            this.RodzajCol.HeaderText = "Rodzaj";
            this.RodzajCol.Name = "RodzajCol";
            this.RodzajCol.ReadOnly = true;
            // 
            // GrupaCol
            // 
            this.GrupaCol.HeaderText = "Grupa";
            this.GrupaCol.Name = "GrupaCol";
            this.GrupaCol.ReadOnly = true;
            // 
            // ProwizjaCol
            // 
            this.ProwizjaCol.HeaderText = "Prowizja";
            this.ProwizjaCol.Name = "ProwizjaCol";
            this.ProwizjaCol.ReadOnly = true;
            // 
            // SumaCol
            // 
            this.SumaCol.HeaderText = "Suma";
            this.SumaCol.Name = "SumaCol";
            // 
            // PolisyForm
            // 
            this.AcceptButton = this.zapiszBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.CancelButton = this.anulujBtn;
            this.ClientSize = new System.Drawing.Size(558, 581);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.grupaPolisy);
            this.Controls.Add(this.hintList);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PolisyForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Szczegóły Polisy";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PolisyForm_FormClosing);
            this.Load += new System.EventHandler(this.PolisyForm_Load);
            this.Click += new System.EventHandler(this.PolisyForm_Click);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.przedmiotyGrid)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ratyGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.klienciBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView hintList;
        private System.Windows.Forms.ColumnHeader Nazwisko;
        private System.Windows.Forms.ColumnHeader Imie;
        private System.Windows.Forms.ColumnHeader Firma;
        private System.Windows.Forms.ColumnHeader PESEL;
        private System.Windows.Forms.ColumnHeader REGON;
        private System.Windows.Forms.ColumnHeader Ulica;
        private System.Windows.Forms.ColumnHeader Miejscowosc;
        private System.Windows.Forms.ColumnHeader Kod_pocztowy;
        private Controls.TxtBox txtSearch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private databaseDataSetTableAdapters.KlienciTableAdapter klienciTableAdapter;
        private System.Windows.Forms.BindingSource klienciBindingSource;
        private databaseDataSet databaseDataSet1;
        private databaseDataSetTableAdapters.KlienciTableAdapter klienciTableAdapter1;
        private databaseDataSetTableAdapters.PolisyTableAdapter polisyTableAdapter1;
        private databaseDataSetTableAdapters.ProdKlientaTableAdapter prodKlientaTableAdapter1;
        private databaseDataSetTableAdapters.ProduktyTableAdapter produktyTableAdapter1;
        private databaseDataSetTableAdapters.ProdKlientaTableAdapter prodKlientaTableAdapter2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button anulujBtn;
        private System.Windows.Forms.Button zapiszBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListView grupaPolisy;
        private System.Windows.Forms.ColumnHeader Prowizja;
        private System.Windows.Forms.ColumnHeader Opis;
        private Controls.TxtBox txtTyp;
        private Controls.RichTxt uwagi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RadioButton wznowionaRadio;
        private System.Windows.Forms.RadioButton nowaRadio;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView ratyGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn wys_raty;
        private System.Windows.Forms.DataGridViewTextBoxColumn term_zaplaty;
        private System.Windows.Forms.DataGridViewCheckBoxColumn zaplacono;
        private System.Windows.Forms.Label label11;
        private Controls.TxtBox skladka;
        private System.Windows.Forms.Label label10;
        private Controls.TxtBox raty;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dataDo;
        private System.Windows.Forms.DateTimePicker dataOd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dataZawarcia;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView przedmiotyGrid;
        private databaseDataSetTableAdapters.GrupyTableAdapter grupyTableAdapter1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListView przedmiotyList;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button button4;
        private databaseDataSetTableAdapters.PrzedmiotyTableAdapter przedmiotyTableAdapter1;
        private databaseDataSetTableAdapters.PrzedmiotyGridViewTableAdapter przedmiotyGridViewTableAdapter1;
        private System.Windows.Forms.Label label7;
        private Controls.TxtBox txtBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RodzajCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn GrupaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProwizjaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn SumaCol;
    }
}