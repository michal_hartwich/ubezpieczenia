﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using StaticRegistry;

namespace Elements
{
    public class Polisa
    {
        public String Id_klienta;
        public String Nazwa;

        public void DodajPolise()
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("INSERT INTO [Polisy] (Id_klienta, Nazwa) VALUES (@Id_klienta, @Nazwa);", conn);
                command.Parameters.AddWithValue("@Id_klienta", this.Id_klienta);
                command.Parameters.AddWithValue("@Nazwa", this.Nazwa);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void EdytujPolise(int id)
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("UPDATE [Polisy] SET Id_klienta=@Id_klienta, Nazwa=@Nazwa WHERE Id_polisy=@Id_polisy;", conn);
                command.Parameters.AddWithValue("@Id_polisy", id);
                command.Parameters.AddWithValue("@Id_klienta", this.Id_klienta);
                command.Parameters.AddWithValue("@Nazwa", this.Nazwa);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public static void UsunPolise(String id)
        {
            using (SqlConnection conn = new SqlConnection(Registry.ConnString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("DELETE FROM Polisy WHERE Id_polisy='" + id + "';", conn);
                //command.Parameters.AddWithValue("@Id_produktu", int.Parse(id));

                command.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}
